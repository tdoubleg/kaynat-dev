import { Component } from '@angular/core';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';
import { Subject } from 'rxjs/internal/Subject';

import { FilterComponent } from '../../shared/component/filter/filter.component';
import { Filter } from '../../shared/component/filter/models/filter';
import { LatLng, Product } from '../../shared/services/product/models/product';
import { ProductService } from '../../shared/services/product/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage {

  public products: Product[];
  public hasLocation: boolean;
  public filter: Filter;
  public isLoading: boolean;

  private destroy$: Subject<void>;

  constructor(private productService: ProductService,
              private modalCtrl: ModalController,
              private routerOutlet: IonRouterOutlet) {
    this.hasLocation = true;
    this.isLoading = true;

    this.filter = JSON.parse(localStorage.getItem('filter'));
    if (this.filter) {
      this.getProductsForLocation(this.filter);
    } else {
      this.hasLocation = false;
    }
  }

  public ionViewWillEnter(): void {
    this.destroy$ = new Subject<void>();
  }

  public ionViewWillLeave(): void {
    this.destroy$.next();
    this.destroy$ = undefined;
  }

  public async getProductsForLocation(filter: Filter): Promise<void> {
    this.isLoading = true;
    if (filter) {
      const latLng: LatLng = filter.geocoderResult.geometry.location as any;
      this.products = await this.productService.getAllProductsForRange(latLng, filter.radius);
    }

    this.hasLocation = !!filter;
    this.isLoading = false;
  }

  public async showFilter(): Promise<void> {
    const modal: HTMLIonModalElement = await this.modalCtrl.create({
      component: FilterComponent,
      cssClass: 'filter-modal',
      swipeToClose: true,
      backdropDismiss: true,
      presentingElement: this.routerOutlet.nativeEl,
    });

    modal.onWillDismiss().then((response: OverlayEventDetail) => {
      if (response.data) {
        this.filter = JSON.parse(localStorage.getItem('filter'));
        this.getProductsForLocation(this.filter);
      }
    });

    return await modal.present();

  }
}
