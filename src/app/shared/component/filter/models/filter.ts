import GeocoderResult = google.maps.GeocoderResult;

export interface Filter {
  geocoderResult: GeocoderResult;
  radius: number;
}