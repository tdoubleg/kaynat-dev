import * as admin from 'firebase-admin';
import DocumentData = admin.firestore.DocumentData;
import FieldValue = admin.firestore.FieldValue;
import Firestore = admin.firestore.Firestore;
import QueryDocumentSnapshot = admin.firestore.QueryDocumentSnapshot;

export function updateItemOnFavorise(db: Firestore, change: QueryDocumentSnapshot): void {
  const data: DocumentData = change.data();
  db.doc(`products/${data.id}`).set({ favoriteCount: FieldValue.increment(1) }, { merge: true });
}

export function updateItemOnDeleteFavorise(db: Firestore, change: QueryDocumentSnapshot): void {
  const data: DocumentData = change.data();
  db.doc(`products/${data.id}`).set({ favoriteCount: FieldValue.increment(-1) }, { merge: true });
}
