import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared-module/shared.module';

import { UserProfileComponent } from './user-profile.component';

@NgModule({
  declarations: [
    UserProfileComponent,
  ],
  imports: [
    SharedModule,
  ],
  exports: [
    UserProfileComponent,
  ],
})
export class UserProfileModule {
}
