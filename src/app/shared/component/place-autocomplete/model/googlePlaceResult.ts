export interface GooglePlaceResult {
  description: string;
  id: string;
  matched_substrings: MatchedSubstring[];
  place_id: string;
  reference: string;
  structured_formatting: StructuredFormatting;
  terms: Term[];
  types: string[];
}

export interface GeoPoint {
  lat: string;
  lng: string;
}

export interface Address {
  city?: string;
  country?: string;
  street?: string;
  streetNr?: string;
  sublocality?: string;
  zip?: string;
}

interface Term {
  offset: number;
  value: string;
}

interface StructuredFormatting {
  main_text: string;
  main_text_matched_substrings: MatchedSubstring[];
  secondary_text: string;
}

interface MatchedSubstring {
  length: number;
  offset: number;
}
