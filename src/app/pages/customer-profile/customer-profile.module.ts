import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ItemViewModule } from '../../shared/component/item-view/item-view.module';
import { SharedModule } from '../../shared/shared-module/shared.module';

import { CustomerProfilePageRoutingModule } from './customer-profile-routing.module';

import { CustomerProfilePage } from './customer-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerProfilePageRoutingModule,
    ItemViewModule,
    SharedModule,
  ],
  declarations: [CustomerProfilePage]
})
export class CustomerProfilePageModule {}
