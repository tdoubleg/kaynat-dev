import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared-module/shared.module';

import { PlaceAutocompleteComponent } from './place-autocomplete.component';

@NgModule({
  declarations: [PlaceAutocompleteComponent],
  imports: [
    SharedModule,
  ],
  exports: [
    PlaceAutocompleteComponent,
  ],
})
export class PlaceAutocompleteModule {
}
