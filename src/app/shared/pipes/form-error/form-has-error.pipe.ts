import { Pipe, PipeTransform } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Pipe({
  pure: false,
  name: 'formHasError',
})
export class FormHasErrorPipe implements PipeTransform {

  public transform(form: AbstractControl): boolean {
    return form.touched && form.dirty && form.errors !== null;
  }
}
