import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss'],
})
export class PrivacyPolicyComponent {

  constructor(private modalCtrl: ModalController) { }

  public close(): void {
    this.modalCtrl.dismiss();
  }

}
