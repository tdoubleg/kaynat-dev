import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';

import { GeneralService } from '../../shared/services/general/general.service';
import { GeolocationService } from '../../shared/services/geolocation/geolocation.service';
import { GeolocationErrorCodes } from '../../shared/services/geolocation/models/geolocation-error-codes.enum';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  @ViewChild('slides', { static: true }) public slider: IonSlides;
  public slideOpts: {};
  public isWaitingForLocation: boolean;
  public isWaitingForNotification: boolean;

  constructor(private geolocationService: GeolocationService,
              private router: Router,
              private generalService: GeneralService) {
  }

  public ngOnInit(): void {
    this.slideOpts = {
      centeredSlides: true,
      centeredSlidesBounds: true,
    };

    this.slider.ionSlideDidChange.subscribe(async () => {
      const activeIndex: number = await this.slider.getActiveIndex();
      if (activeIndex === 1 ) {
        this.slider.lockSwipeToNext(true);
      } else {
        this.slider.lockSwipeToNext(false);
      }
    });
  }

  public async permitLocation(): Promise<void> {
    this.isWaitingForLocation = true;
    try {
      await this.geolocationService.getCurrentPosition();
    } catch (e) {
      if (e.code === GeolocationErrorCodes.NO_RESPONSE) {
        await this.generalService.presentInfoAlert('location.error.header', 'location.error.noResponseMessage');
      }
    } finally {
      await this.slider.lockSwipeToNext(false);
      await this.slider.slideNext();
      this.isWaitingForLocation = false;
    }
  }

  public permitNotifications(): void {
    console.debug('permitNotification');
  }

  public showPrivacy(): void {
    console.debug('showPrivacy');
  }

  public register(): void {
    this.router.navigateByUrl('login');
    localStorage.setItem('welcomeDone', 'true');

  }

  public skip(): void {
    this.router.navigateByUrl('/');
    localStorage.setItem('welcomeDone', 'true');
  }
}
