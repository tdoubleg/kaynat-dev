export enum RequestStatusEnum {
  DECLINED = 0,
  ACCEPTED = 1,
  OPEN = 2,
}
