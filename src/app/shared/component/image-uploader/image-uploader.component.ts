import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';

import { Image } from './models/image';
import { ImagePopoverComponent } from './partials/image-popover/image-popover.component';
import { ImageService } from './services/image.service';

// tslint:disable:no-any
@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss'],
})
export class ImageUploaderComponent implements OnInit {

  public sliderOptions: unknown;
  public showPager: boolean;
  public _images: Image[];

  @Input() public isProfileImage: boolean;
  @Input() public size: number = 5;
  @Output() public profileImageChanged: EventEmitter<void>;
  @Output() public imageDeleted: EventEmitter<void>;
  @ViewChild('slider') private slider: IonSlides;

  constructor(private modalCtrl: ModalController, private imageService: ImageService) {
    this.sliderOptions = {
      slidesPerView: 'auto',
    };

    this.profileImageChanged = new EventEmitter<void>();
    this.imageDeleted = new EventEmitter<void>();
  }

  @Input()
  public set images(images: string[]) {
    const filteredImages: string[] = images.filter((url: string) => !!url);
    if (Array.isArray(filteredImages) && filteredImages.length > 0) {
      const mappedImages: Image[] = filteredImages.map((url: string) => ({ url, loaded: false }));
      this.imageService.images = mappedImages;
      this._images = mappedImages;
    } else {
      this._images = undefined;
    }
  }

  public async ngOnInit(): Promise<void> {
    await this.allowSliderMove();
  }

  public async selectImage(): Promise<void> {
    if (!this._images) {
      this._images = [];
    }
    try {
      this._images = await this.imageService.takePicture();
      if (this.isProfileImage) {
        this.profileImageChanged.emit();
      }
      await this.allowSliderMove();
    } catch (e) {
      console.error('Error while taking Picture', e);
    }
  }

  public async deleteImage(index: number): Promise<void> {
    this._images.splice(index, 1);
    this.imageService.images = this._images;
    this.imageDeleted.emit();
    await this.allowSliderMove();
  }

  public async openImage(url: string, index: number): Promise<void> {
    const modal: HTMLIonModalElement = await this.modalCtrl.create({
      component: ImagePopoverComponent,
      componentProps: { index, url },
    });
    modal.animated = true;
    await modal.present();

    await modal.onDidDismiss().then((response: any) => {
      const data: any = response.data;
      if (data && data.shouldDelete) {
        this.deleteImage(index);
      }
    });
  }

  private async allowSliderMove(): Promise<void> {
    if (this.slider) {
      const swiper: any = await this.slider.getSwiper();
      const timeout: number = 100;

      setTimeout(() => {
        const bullets: number = swiper.pagination.bullets.length;
        this.slider.lockSwipes(bullets <= 1);
      }, timeout);
    }
  }
}
