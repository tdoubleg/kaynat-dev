import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { GeolocationPosition } from '@capacitor/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { debounceTime, distinctUntilChanged, filter, tap } from 'rxjs/operators';

import { GeneralService } from '../../services/general/general.service';
import { GeolocationService } from '../../services/geolocation/geolocation.service';

import { AutocompleteService } from './services/autocomplete/autocomplete.service';
import GeocoderResult = google.maps.GeocoderResult;
import LatLng = google.maps.LatLng;

const DEBOUNCE_TIME: number = 1000;
const MIN_CHAR_AMOUNT: number = 3;

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-place-autocomplete',
  templateUrl: './place-autocomplete.component.html',
  styleUrls: ['./place-autocomplete.component.scss'],
})
export class PlaceAutocompleteComponent implements OnInit {
  @Output() public onSelectedLocation: EventEmitter<GeocoderResult>;
  @Input() public selectedLocation: GeocoderResult;

  public locations: GeocoderResult[];
  public searchTerm$: BehaviorSubject<string>;
  public isLoading: boolean;

  constructor(private autocompleteService: AutocompleteService,
              private geolocationService: GeolocationService,
              private generalService: GeneralService,
              private cdr: ChangeDetectorRef) {
    this.searchTerm$ = new BehaviorSubject<string>(null);
    this.onSelectedLocation = new EventEmitter();

    this.onSearch();
  }

  public async ngOnInit(): Promise<void> {
    if (!this.selectedLocation) {
      await this.getCurrentLocation();
    }
  }

  public onSearch(): void {
    this.searchTerm$.pipe(
      filter((term: string) => !!term),
      tap((term: string) => this.isLoading = term.length > MIN_CHAR_AMOUNT),
      filter((term: string) => term.length > MIN_CHAR_AMOUNT),
      debounceTime(DEBOUNCE_TIME),
      distinctUntilChanged(),
    ).subscribe(async (term: string) => {
      try {
        this.locations = await this.autocompleteService.geocode(term);
      } catch (e) {
        this.locations = [];
      } finally {
        this.isLoading = false;
        this.cdr.detectChanges();
      }
    });
  }

  public async getCurrentLocation(): Promise<void> {
    this.isLoading = true;
    try {
      const position: GeolocationPosition = await this.geolocationService.getCurrentPosition();
      try {
        const location: LatLng = new LatLng(position.coords.latitude, position.coords.longitude);
        this.locations = await this.autocompleteService.geocode(location);
        this.setLocation(this.locations[0]);
      } catch (e) {
        this.locations = [];
      }
    } catch (e) {
      console.error('geolocationPosition', e);
      if (e.code === 1) {
        await this.generalService.presentInfoAlert('location.error.header', 'location.error.denied');
      } else {
        await this.generalService.presentInfoAlert('location.error.header', 'location.error.noResponseMessage');
      }
      this.locations = [];
    } finally {
      this.isLoading = false;
      this.cdr.detectChanges();
    }
  }

  public setLocation(location: GeocoderResult): void {
    this.onSelectedLocation.emit(location);
    this.selectedLocation = location;
  }

  public clearInput(): void {
    this.searchTerm$.next(null);
    this.onSelectedLocation.emit(null);
    this.selectedLocation = null;
    this.locations = null;
  }
}
