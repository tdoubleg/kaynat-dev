import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxFirebaseAuthService } from 'ngx-firebase-auth';

import { ImageService } from '../../../../shared/component/image-uploader/services/image.service';
import { GeneralService } from '../../../../shared/services/general/general.service';
import { User } from '../../../../shared/services/user/models/user';
import { UserService } from '../../../../shared/services/user/user.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnChanges {
  @Input() public user: User;

  public form: FormGroup;
  public isLoading: boolean;
  public imageChanged: boolean;
  public imageDeleted: boolean;

  constructor(private generalService: GeneralService,
              private userService: UserService,
              private authService: NgxFirebaseAuthService,
              private imageService: ImageService,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (this.user) {
      this.initForm();
    }
  }

  public updateEmail(): void {
    if (!this.form.get('email').untouched) {
      this.user = {
        ...this.user,
        email: this.form.get('email').value,
      };

      this.generalService.verfiyAlert(async (result: { [key: string]: string }) => {
        this.isLoading = true;

        try {
          await this.userService.updateEMail(result.password, this.user.email);
          this.generalService.setLogEvent('on_email_changed', {});
          this.isLoading = false;
          await this.generalService.presentToast('profile.user.edit.email.success', 'middle');
        } catch (e) {
          console.error('E-Mail Update error: ', e);
          this.isLoading = false;
          await this.generalService.presentInfoAlert('general.verify.header', 'profile.user.edit.email.fail');
        }
      });
    }
  }

  public async updateUserInfo(): Promise<void> {
    this.isLoading = true;
    try {
      if (this.imageChanged) {
        try {
          this.user.profileImageUrl = await this.userService.setProfileImage(this.user.id);
        } catch (e) {
          console.error('Profile Image Update', e);
          await this.generalService.presentInfoAlert(
            'profile.account.update.error.header',
            'profile.account.update.error.message',
          );
        }
      } else if (this.imageDeleted) {
        this.user.profileImageUrl = null;
      }

      this.user = { ...this.user, ...this.form.value };
      await this.userService.setUserInformation(this.user);
      await this.generalService.presentInfoAlert(
        'profile.account.update.success.header',
        'profile.account.update.success.message',
      );
      this.reset();
    } catch (e) {
      console.error('setUserInformatio() Error', e);
      await this.generalService.presentInfoAlert(
        'profile.account.update.error.header',
        'profile.account.update.error.message',
      );
      this.isLoading = false;
    }
  }

  public async logout(): Promise<void> {
    await this.authService.logout();
    await this.router.navigateByUrl('/');
    this.userService.user$.next(undefined);
  }

  private initForm(): void {
    this.form = this.formBuilder.group({
      firstName: [{ value: this.user.firstName ? this.user.firstName : '', disabled: this.user.firstName }],
      lastName: [{ value: this.user.lastName ? this.user.lastName : '', disabled: this.user.lastName }],
      email: [this.user.email, Validators.compose([Validators.email, Validators.required])],
    });
  }

  private reset(): void {
    this.isLoading = false;
    this.imageDeleted = false;
    this.imageChanged = false;
  }
}
