import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthenticationGuard } from '../core/guards/authentication.guard';

import { TabsPage } from './tabs.page';

//tslint:disable
const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../pages/home/home.module').then(m => m.HomePageModule),
      },
      {
        path: 'detail',
        loadChildren: () => import('../pages/detail/detail.module').then(m => m.DetailPageModule),
      },
      {
        path: 'customer-profile',
        loadChildren: () => import('../pages/customer-profile/customer-profile.module').then(m => m.CustomerProfilePageModule),
      },
      {
        path: 'create',
        loadChildren: () => import('../pages/create/create.module').then(m => m.CreatePageModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'orders',
        loadChildren: () => import('../pages/order/order.module').then(m => m.OrderPageModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'profile',
        loadChildren: () => import('../pages/profile/profile.module').then(m => m.ProfilePageModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'chat',
        loadChildren: () => import('../pages/chat/chat.module').then(m => m.ChatModule),
        canActivate: [AuthenticationGuard],
      },
      {
        path: 'settings',
        loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsPageModule),
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {
}
