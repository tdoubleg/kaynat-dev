import { firestore, messaging } from 'firebase-admin';
import { Change } from 'firebase-functions';

import { createChat } from './chat';
import { RequestStatusEnum } from './enums/request-status.enum';
import { Request } from './models/request';
import { notify } from './notification';
import Firestore = firestore.Firestore;
import QueryDocumentSnapshot = firestore.QueryDocumentSnapshot;
import MessagingPayload = messaging.MessagingPayload;
import MessagingDevicesResponse = messaging.MessagingDevicesResponse;

export function setStatusOnCreate(db: Firestore, snapshot: QueryDocumentSnapshot): Promise<FirebaseFirestore.WriteResult | void> {
  return snapshot.ref.set({ status: 2 }, { merge: true }).then(async () => {
    await createChat(db, snapshot);

    const data: Request = snapshot.data() as Request;

    if (data) {
      const payload: MessagingPayload = {
        notification: {
          title: 'Neue Anfrage',
          body: `Du hast eine neue Anfrage zu ${data.item.name}`,
        },
        data: {
          chatId: snapshot.id,
        },
      };

      await notify(data.ownerId, payload);
    }

  }).catch((error: unknown) => {
    console.error('error while setting request status', error);
  });
}

export async function setStatusOnUpdate(snapshot: Change<QueryDocumentSnapshot>): Promise<MessagingDevicesResponse | null> {
  const data: Request = snapshot.after.data() as Request;

  if (data) {
    const status: string = data.status === RequestStatusEnum.DECLINED ? 'leider abgelehnt' : 'angenommen';

    const payload: MessagingPayload = {
      notification: {
        title: 'Deine Anfrage',
        body: `Deine Anfrage zu ${data.item.name} wurde ${status}`,
      },
      data: {
        chatId: snapshot.after.id,
      },
    };

    return notify(data.fromUser.id, payload);
  }

  return null;
}
