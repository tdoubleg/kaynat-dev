import { Injectable } from '@angular/core';

import { Address, GeoPoint } from '../../model/googlePlaceResult';

import { GoogleAddressComponent } from './model/googleAddressComponent';

@Injectable({
  providedIn: 'root',
})
export class GeocodeService {
  // private geocoder: google.maps.Geocoder;

  constructor() {
    // this.geocoder = new google.maps.Geocoder();
  }

  // public geocodeAddress(location: GeoPoint): void {
  //   this.geocoder.geocode({ location }, (results: any, status: any) => {
  //     if (status === google.maps.GeocoderStatus.OK) {
  //       const addressComponents: GoogleAddressComponent[] = results[0].address_components;
  //       let address: Address = {};
  //       address.sublocality = '';
  //       address.street = '';
  //       address.streetNr = '';
  //
  //       addressComponents.forEach((addressComponent: GoogleAddressComponent) => {
  //         if (addressComponent.types[0] === 'street_number') {
  //           address = { ...address, streetNr: addressComponent.long_name };
  //         } else if (addressComponent.types[0] === 'sublocality_level_2') {
  //           address = { ...address, sublocality: addressComponent.long_name };
  //         } else if (addressComponent.types[0] === 'route') {
  //           address = { ...address, street: addressComponent.long_name };
  //         } else if (addressComponent.types[0] === 'locality') {
  //           address = { ...address, city: addressComponent.long_name };
  //         } else if (addressComponent.types[0] === 'postal_code') {
  //           address = { ...address, zip: addressComponent.long_name };
  //         } else if (addressComponent.types[0] === 'country') {
  //           address = { ...address, country: addressComponent.long_name };
  //         }
  //       });
  //     } else {
  //       console.log('Error - ', results, ' & Status - ', status);
  //     }
  //   });
  // }
  //
  // public codeAddress(address: string): Observable<any> {
  //   this.geocoder.geocode({ address }, (results: any, status: any) => {
  //     if (status === google.maps.GeocoderStatus.OK) {
  //     } else {
  //       console.log('Error - ', results, ' & Status - ', status);
  //     }
  //   });
  // }

}
