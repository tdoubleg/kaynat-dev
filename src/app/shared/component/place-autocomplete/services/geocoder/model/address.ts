export interface Address {
  city?: string;
  country?: string;
  street?: string;
  streetNr?: string;
  sublocality?: string;
  zip?: string;
}
