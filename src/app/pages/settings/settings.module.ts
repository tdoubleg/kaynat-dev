import { NgModule } from '@angular/core';

import { PrivacyModule } from '../../shared/component/privacy/privacy.module';
import { SharedModule } from '../../shared/shared-module/shared.module';

import { LegalComponent } from './partials/legal/legal.component';
import { ImprintComponent } from './partials/legal/partials/imprint/imprint.component';
import { PrivacyPolicyComponent } from './partials/legal/partials/privacy-policy/privacy-policy.component';
import { TermsComponent } from './partials/legal/partials/terms/terms.component';
import { SettingsPageRoutingModule } from './settings-routing.module';
import { SettingsPage } from './settings.page';

@NgModule({
  imports: [
    SharedModule,
    SettingsPageRoutingModule,
    PrivacyModule,
  ],
  declarations: [
    SettingsPage,
    LegalComponent,
    ImprintComponent,
    TermsComponent,
    PrivacyPolicyComponent,
  ],
})
export class SettingsPageModule {
}
