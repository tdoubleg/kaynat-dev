import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { GoogleMap } from '@angular/google-maps';
import { ModalController } from '@ionic/angular';

import { Filter } from './models/filter';
import Circle = google.maps.Circle;
import GeocoderResult = google.maps.GeocoderResult;

const THOUSAND: number = 1000;

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements AfterViewInit {
  @ViewChild(GoogleMap) public map: GoogleMap;

  public filter: Filter;
  public radiusValue: number;
  public circle: Circle;
  public options: google.maps.MapOptions = {
    zoomControl: false,
    disableDoubleClickZoom: true,
    disableDefaultUI: true,
    maxZoom: 15,
    minZoom: 1,
  };

  constructor(private modalCtrl: ModalController) {
    this.filter = JSON.parse(localStorage.getItem('filter'));
    if (!this.filter) {
      this.filter = { radius: 1, geocoderResult: null };
    } else {
      this.radiusValue = this.filter.radius;
    }

  }

  public ngAfterViewInit(): void {
    if (this.filter.geocoderResult) {
      this.setCircle();
    }
  }

  public setRadius(): void {
    let tempRadius: number = this.filter.radius;
    this.radiusValue = tempRadius;
    if (tempRadius === 0) {
      tempRadius = 1;
      this.radiusValue = 1;
    }

    this.circle.setRadius(tempRadius * THOUSAND);
    this.map.fitBounds(this.circle.getBounds());
    this.filter = { ...this.filter, radius: tempRadius };
  }

  public setLocation(geocoderResult: GeocoderResult): void {
    if (geocoderResult) {
      this.filter = { ...this.filter, geocoderResult };
      if (!this.circle) {
        this.setCircle();
      } else {
        this.circle.setCenter(this.filter.geocoderResult.geometry.location);
      }
    }
  }

  public setCircle(): void {
    this.circle = new google.maps.Circle({
      strokeColor: '#fca652',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#fca652',
      fillOpacity: 0.35,
      map: this.map.googleMap,
      center: this.filter.geocoderResult.geometry.location,
      radius: this.filter.radius * THOUSAND,
    });

    setTimeout(() => {
      this.map.fitBounds(this.circle.getBounds());
    }, 50);
  }

  public async close(confirmed: boolean): Promise<void> {
    if (confirmed) {
      localStorage.setItem('filter', JSON.stringify(this.filter));
    }
    await this.modalCtrl.dismiss(confirmed);
  }
}
