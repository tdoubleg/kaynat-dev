export interface Privacy {
  analytics: boolean;
  performance: boolean;
  crashlytics: boolean;
  personalizedAds: boolean;
}
