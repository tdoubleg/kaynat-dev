import { Product } from '../../../shared/services/product/models/product';

export interface Chat {
  requestId: string;
  createdAt: number;
  ownerId: string;
  createdBy: ChatUser;
  messages: Message[];
  item: Partial<Product>;
}

export interface ChatUser {
  id: string;
  profileImageUrl: string;
  username: string;
}

export interface Message {
  content: string;
  createdAt: number;
  user: ChatUser;
}
