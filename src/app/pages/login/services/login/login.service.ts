import { Injectable } from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import firebase from 'firebase';
import { NgxFirebaseAuthService } from 'ngx-firebase-auth';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

import { GeneralService } from '../../../../shared/services/general/general.service';
import { IAlertButton, IAlertInput } from '../../../../shared/services/general/models/alertElements';
import FirebaseError = firebase.FirebaseError;

@Injectable({
  providedIn: 'root',
})
export class LoginService {

  constructor(private translateService: TranslateService,
              private authService: NgxFirebaseAuthService,
              private generalService: GeneralService) {
  }

  public async getResetForm(emailForm: AbstractControl): Promise<void> {
    const inputs: IAlertInput[] = [
      {
        name: 'email',
        type: 'text',
        cssClass: 'reset-password-input',
        placeholder: this.translateService.instant('login.form.email.placeholder'),
        value: emailForm.value,
      },
    ];
    const buttons: IAlertButton[] = [
      {
        text: this.translateService.instant('login.passwordReset.confirm'),
        role: 'confirm',
        cssClass: 'reset-password-button',
        handler: async (value: any) => {
          if (value.email) {
            try {
              await this.authService.sendPasswordResetEmail(emailForm.value);
            } catch (e) {
              const error: FirebaseError = e as FirebaseError;
              await this.generalService.presentFirebaseErrorAlert(error);
            }
          }
        },
      },
    ];

    await this.generalService.showPromptAlert('login.passwordReset.header', inputs, buttons);
    const resetBtn: HTMLButtonElement = document.querySelector('.reset-password-button');
    const emailInput: HTMLInputElement = document.querySelector('.reset-password-input');
    const emailInput$: BehaviorSubject<string> = new BehaviorSubject<string>(emailInput.value);
    emailInput.addEventListener('keyup', () => {
      emailForm.setValue(emailInput.value);
      emailInput$.next(emailInput.value);
    });

    emailInput$.asObservable().subscribe((value: string) => {
      if (value.length > 0 && Validators.email(emailForm) === null) {
        resetBtn.disabled = false;
        resetBtn.style.color = '#fca652';
      } else {
        resetBtn.disabled = true;
        resetBtn.style.color = 'lightgrey';
      }
    });
  }
}
