import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

import { User } from '../../shared/services/user/models/user';
import { UserService } from '../../shared/services/user/user.service';
import { RequestService } from '../detail/service/request/request.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
  public currentState: string;
  public user: User;
  public numberOfUnreadRequests$: Subject<number>;

  private destroy$: Subject<void>;

  constructor(private userService: UserService,
              private requestService: RequestService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.currentState = 'requests';
    this.destroy$ = new Subject<void>();
  }

  public ngOnInit(): void {
    this.userService.user$
      .pipe(takeUntil(this.destroy$))
      .subscribe((user: User) => {
        this.user = user;
      });

    this.activatedRoute.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe((params: Params) => {
        this.currentState = params.tab ?? 'requests';
      });

    this.numberOfUnreadRequests$ = this.requestService.numberOfUnreadRequests$;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public segmentChanged(segment: CustomEvent): void {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: { tab: segment.detail.value },
      });
  }

}
