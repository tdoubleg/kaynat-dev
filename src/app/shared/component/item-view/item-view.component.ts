import { Component, Input } from '@angular/core';

import { Product } from '../../services/product/models/product';

@Component({
  selector: 'app-item-view',
  templateUrl: './item-view.component.html',
  styleUrls: ['./item-view.component.scss'],
})
export class ItemViewComponent {
  @Input() public item: Product;
  @Input() public showFavoriteCount: boolean;
  @Input() public showTimestamp: boolean;
  @Input() public showUserInfo: boolean;

  constructor() {
    this.showFavoriteCount = false;
    this.showTimestamp = false;
    this.showUserInfo = true;
  }
}
