import { User } from '../../user/models/user';

export interface BaseProduct {
  name: string;
  description: string;
  portion: Portion;
  rating: Rating;
  favoriteCount: number;
  user: Partial<User>;
  timestamp: number;
}

export interface Product extends BaseProduct {
  id: string;
  images: string[];
  location: Location;
  defaultImage?: string;
  status: ProductStatusEnum;
}

export interface Portion {
  unit: ProductUnitEnum;
  size: number;
  price: number;
}

export interface Rating {
  average: number;
  rates: number;
}

export interface Location {
  hash: string;
  value: string;
  lat: number;
  lng: number;
}

export enum ProductStatusEnum {
  UNPUBLISHED,
  PUBLISHED,
  DELETED,
}

export enum ProductUnitEnum {
  g,
  kg,
  l,
  ml,
  pc,
}

export interface LatLng {
  lat: number;
  lng: number;
}
