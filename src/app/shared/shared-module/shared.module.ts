import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { FormHasErrorPipe } from '../pipes/form-error/form-has-error.pipe';
import { LocaleDatePipe } from '../pipes/locale-date/locale-date.pipe';
import { ProductService } from '../services/product/product.service';

@NgModule({
  declarations: [
    FormHasErrorPipe,
    LocaleDatePipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    FlexLayoutModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    FlexLayoutModule,
    FormHasErrorPipe,
    LocaleDatePipe,
  ],
  providers: [
    ProductService,
  ],
})
export class SharedModule {
}
