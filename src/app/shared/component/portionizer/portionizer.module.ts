import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared-module/shared.module';

import { PortionizerComponent } from './portionizer.component';

@NgModule({
  declarations: [PortionizerComponent],
  imports: [
    SharedModule,
  ],
  exports: [
    PortionizerComponent,
  ],
})
export class PortionizerModule {
}
