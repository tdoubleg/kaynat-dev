import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot, CanActivate,
  CanActivateChild,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs/internal/Observable';

//tslint:disable
@Injectable({
  providedIn: 'root',
})
export class WelcomeGuard implements CanActivateChild, CanActivate {
  constructor(private navCtrl: NavController) {

  }

  public canActivateChild(next: ActivatedRouteSnapshot,
                          state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const welcomeDone: boolean = Boolean(localStorage.getItem('welcomeDone'));
    if (!welcomeDone) {
      this.navCtrl.navigateForward('welcome');
    }

    return welcomeDone;
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const welcomeDone: boolean = Boolean(localStorage.getItem('welcomeDone'));
    if (welcomeDone) {
      this.navCtrl.navigateForward('tabs/home');
    }

    return !welcomeDone;
  }
}
