import { NgModule } from '@angular/core';
import { FilterModule } from '../../shared/component/filter/filter.module';

import { ImageUploaderModule } from '../../shared/component/image-uploader/image-uploader.module';
import { ItemViewModule } from '../../shared/component/item-view/item-view.module';
import { PlaceAutocompleteComponent } from '../../shared/component/place-autocomplete/place-autocomplete.component';
import { SharedModule } from '../../shared/shared-module/shared.module';

import { HomePageRoutingModule } from './home-routing.module';
import { HomePage } from './home.page';
import { ProductSliderComponent } from './partials/product-slider/product-slider.component';
import { RecommendedSliderComponent } from './partials/recommended-slider/recommended-slider.component';

@NgModule({
  imports: [
    SharedModule,
    HomePageRoutingModule,
    ImageUploaderModule,
    ItemViewModule,
    FilterModule,
  ],
  declarations: [
    HomePage,
    ProductSliderComponent,
    RecommendedSliderComponent,
  ],
})
export class HomePageModule {
}
