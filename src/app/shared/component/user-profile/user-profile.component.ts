import { Component, Input } from '@angular/core';
import { BaseUser } from '../../services/user/models/user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent {

  @Input() public user: BaseUser;
}
