import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import firebase from 'firebase';
import { NgxFirebaseAuthService } from 'ngx-firebase-auth';
import { AuthContext } from 'ngx-firebase-auth/lib/models/authContext';

import { GeneralService } from '../../../../shared/services/general/general.service';
import { User } from '../../../../shared/services/user/models/user';
import { UserService } from '../../../../shared/services/user/user.service';
import { LoginService } from '../../services/login/login.service';
import UserCredential = firebase.auth.UserCredential;
import FirebaseError = firebase.FirebaseError;

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
})
export class RegisterFormComponent implements OnInit {
  public form: FormGroup;
  public isLoading: boolean;

  constructor(private loginService: LoginService,
              private generalService: GeneralService,
              private authService: NgxFirebaseAuthService,
              private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private router: Router) {
  }

  public ngOnInit(): void {
    this.initForm();
  }

  public initForm(): void {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.required],
      username: ['', Validators.required],
    });
  }

  public async register(): Promise<void> {
    this.isLoading = true;
    const authContext: AuthContext = {
      email: this.form.controls.email.value,
      password: this.form.controls.password.value,
    };

    try {
      const userCredentials: UserCredential = await this.authService.register(authContext);

      const { password, ...formValue } = this.form.value;
      const user: User = formValue;
      user.id = userCredentials.user.uid;
      user.active = true;
      user.verified = false;
      user.profileImageUrl = await this.userService.setProfileImage(user.id);

      this.userService.setUserInformation(user).then(async () => {
        if (user) {
          const returnUrl: string = this.activatedRoute.snapshot.queryParams.returnUrl || '/';
          await this.router.navigateByUrl(returnUrl, { replaceUrl: true });
          this.isLoading = false;
        }
      }).catch((e: FirebaseError) => {
        console.error('Error setting user', e);
        this.isLoading = false;
      });
    } catch (e) {
      const error: FirebaseError = e as FirebaseError;
      await this.generalService.presentFirebaseErrorAlert(error);
      this.isLoading = false;
    }
  }
}
