import { Component, Input, OnInit } from '@angular/core';

import { GeneralService } from '../../services/general/general.service';

import { Privacy } from './models/privacy';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss'],
})
export class PrivacyComponent implements OnInit {

  @Input() public showHeader: boolean;

  public privacy: Privacy;

  constructor(private generalService: GeneralService) {
    this.showHeader = true;
  }

  public ngOnInit(): void {
    const privacy: Privacy = JSON.parse(localStorage.getItem('privacy'));
    if (privacy) {
      this.privacy = privacy;
    } else {
      this.privacy = {
        analytics: false,
        performance: false,
        crashlytics: false,
        personalizedAds: false,
      };
    }
  }

  public setAnalytics(checked: boolean): void {
    console.debug('setAnalytics', checked);
    // this.firebase.setAnalyticsCollectionEnabled(checked)
    //   .catch(error => this.firebase.logError('Error enabling Analytics', error));
    // this.privacy.analytics = checked;
    // localStorage.setItem('privacy', JSON.stringify(this.privacy));
  }

  public setPerformance(checked: boolean): void {
    console.debug('setPerformance', checked);
    //
    // this.firebase.setPerformanceCollectionEnabled(checked)
    //   .catch(error => this.firebase.logError('Error enabling Performance Collection', error));
    // this.privacy.performance = checked;
    // localStorage.setItem('privacy', JSON.stringify(this.privacy));
  }

  public setCrashlytics(checked: boolean): void {
    console.debug('setCrashlytics', checked);
    //
    // this.firebase.setCrashlyticsCollectionEnabled(checked)
    //   .catch(error => this.firebase.logError('Error enabling Crashlytics', error));
    //
    // this.privacy.crashlytics = checked;
    // localStorage.setItem('privacy', JSON.stringify(this.privacy));
  }

  public setPersonalizedAds(checked: boolean): void {
    console.debug('checked', checked);

    // this.privacy.personalizedAds = checked;
    // localStorage.setItem('privacy', JSON.stringify(this.privacy));
  }

  public async showAnalyticsInfo(): Promise<void> {
    await this.generalService.presentInfoAlert('settings.privacy.analytics.title',
      'settings.privacy.analytics.description');
  }

  public async showPerformanceInfo(): Promise<void> {
    await this.generalService.presentInfoAlert('settings.privacy.performance.title',
      'settings.privacy.performance.description');
  }

  public async showCrashlyticsInfo(): Promise<void> {
    await this.generalService.presentInfoAlert('settings.privacy.crashlytics.title',
      'settings.privacy.crashlytics.description');
  }

  public async showPersonalizedAdsInfo(): Promise<void> {
    await this.generalService.presentInfoAlert('settings.privacy.personalizedAds.title',
      'settings.privacy.personalizedAds.description');
  }
}
