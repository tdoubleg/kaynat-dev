import { Component, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

import { Product } from '../../../../shared/services/product/models/product';
import { ProductService } from '../../../../shared/services/product/product.service';

const MARGIN: number = 20;

@Component({
  selector: 'app-product-slider',
  templateUrl: './product-slider.component.html',
  styleUrls: ['./product-slider.component.scss'],
})
export class ProductSliderComponent {

  @Input() public title: string;
  @Input() public items: Product[];
  @Input() public type: string;

  public slideOpts: unknown;

  constructor(private productService: ProductService, private navCtrl: NavController) {
    this.slideOpts = {
      speed: 400,
      slidesPerView: 2,
      spaceBetween: 10,
      width: window.innerWidth - MARGIN,
      scrollbar: {
        snapOnRelease: false,
      },
    };
  }

  public async showProductDetail(item: Product): Promise<void> {
    this.productService.selectedItem = item;
    await this.navCtrl.navigateForward(`/tabs/detail/${item.id}`);
  }
}
