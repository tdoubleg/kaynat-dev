import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import firebase from 'firebase';

import { IAlertButton, IAlertInput } from './models/alertElements';
import FirebaseError = firebase.FirebaseError;

@Injectable({
  providedIn: 'root',
})
export class GeneralService {

  constructor(private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController,
              private translateService: TranslateService) {
  }

  public async presentLoading(messageKey: string): Promise<HTMLIonLoadingElement> {
    const loader: HTMLIonLoadingElement = await this.loadingCtrl.create({
      cssClass: 'custom-loading-spinner',
      message: this.translateService.instant(messageKey),
    });
    await loader.present();

    return loader;
  }

  public async presentInfoAlert(headerKey: string, messageKey: string, errorMessage?: string): Promise<void> {
    const error: string = errorMessage ? `${this.translateService.instant('alert.error')} - ${errorMessage}` : '';
    const alert: HTMLIonAlertElement = await this.alertCtrl.create({
      cssClass: 'custom-alert',
      header: this.translateService.instant(headerKey),
      message: `${this.translateService.instant(messageKey)} ${error}`,
      buttons: [
        {
          text: this.translateService.instant('alert.okay'),
          role: 'cancel',
        },
      ],
    });

    await alert.present();
  }

  public async presentConfirmAlertWithHandler(headerKey: string,
                                              messageKey: string,
                                              buttonKey: string,
                                              handler: () => void): Promise<void> {
    const alert: HTMLIonAlertElement = await this.alertCtrl.create({
      cssClass: 'custom-alert',
      header: this.translateService.instant(headerKey),
      message: this.translateService.instant(messageKey),
      buttons: [
        {
          text: this.translateService.instant('alert.cancel'),
          role: 'cancel',
        },
        {
          text: this.translateService.instant(buttonKey ?? 'alert.okay'),
          handler,
        },
      ],
    });

    await alert.present();
  }

  public async presentLoginAlert(handler: () => void): Promise<void> {
    await this.presentConfirmAlertWithHandler(
      'general.login.title',
      'general.login.message',
      'general.login.button',
      handler);
  }

  public async presentAlertCustomButtons(headerKey: string,
                                         messageKey: string,
                                         buttons?: IAlertButton[]): Promise<void> {
    const alert: HTMLIonAlertElement = await this.alertCtrl.create({
      cssClass: 'custom-alert',
      header: this.translateService.instant(headerKey),
      message: this.translateService.instant(messageKey),
      buttons: [
        ...buttons,
        {
          text: 'Cancel',
        },
      ],
    });
    await alert.present();
  }

  public async showPromptAlert(headerKey: string, inputs: IAlertInput[], buttons?: IAlertButton[]): Promise<void> {
    const alert: HTMLIonAlertElement = await this.alertCtrl.create({
      cssClass: 'custom-alert',
      header: this.translateService.instant(headerKey),
      inputs,
      buttons: [
        ...buttons,
        {
          text: this.translateService.instant('alert.cancel'),
          role: 'cancel',
          cssClass: 'danger',
        },
      ],
    });

    await alert.present();
  }

  public async verfiyAlert(successHandler: (result: { [key: string]: string }) => void): Promise<void> {
    const alert: HTMLIonAlertElement = await this.alertCtrl.create({
      header: this.translateService.instant('alert.verify.header'),
      message: this.translateService.instant('alert.verify.message'),
      inputs: [
        {
          name: 'password',
          type: 'password',
          placeholder: this.translateService.instant('alert.verify.placeholder'),
        },
      ],
      buttons: [
        {
          text: this.translateService.instant('alert.cancel'),
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: this.translateService.instant('alert.confirm'),
          handler: successHandler,
        },
      ],
    });

    await alert.present();
  }

  public async presentFirebaseErrorAlert(error: FirebaseError): Promise<void> {
    const headerKey: string = 'login.error.title';
    let messageKey: string;

    const code: string = error.code;
    if (code === 'auth/wrong-password') {
      messageKey = 'login.error.credentials';
    } else if (code === 'auth/weak-password') {
      messageKey = 'login.error.weak';
    } else if (code === 'auth/email-already-in-use') {
      messageKey = 'login.error.already_exists';
    } else if (error.code === 'auth/network-request-failed') {
      messageKey = 'login.error.network';
    } else if (error.code === 'auth/too-many-requests') {
      messageKey = 'login.error.too_many_attempts';
    } else {
      messageKey = 'login.error.credentials';
    }

    await this.presentInfoAlert(headerKey, messageKey);
  }

  public async presentToast(key: string,
                            position: 'top' | 'bottom' | 'middle',
                            duration: number = 5000): Promise<void> {
    const toast: HTMLIonToastElement = await this.toastCtrl.create({
      duration,
      position,
      message: this.translateService.instant(key),
      color: 'primary',
      buttons: [
        {
          text: 'X',
          role: 'cancel',
        },
      ],
    });

    await toast.present();
  }

  public setLogEvent(type: string, data: unknown): void {
    // if (this.platform.is('cordova')) {
    //   this.firebase.logEvent(type, data).catch((error) => {
    //     console.log('Log Event Type', type);
    //     console.log('Set Log Event Data', data);
    //     console.log('Set Log Event Error', error);
    //   });
    // }
  }

  public setScreenName(screen: string): void {
    // if ( this.platform.is('cordova') ) {
    //   this.firebase.setScreenName(screen).catch((error) => {
    //     console.log('Set Screen Name', screen);
    //     console.log('Set Screen Name Error', error);
    //   });
    // }
  }
}
