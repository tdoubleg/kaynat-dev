import { RequestStatusEnum } from '../enums/request-status.enum';

export interface Request {
  fromUser: RequestUser;
  item: RequestItem;
  status: RequestStatusEnum;
  requested: Requested;
  ownerId: string;
  read: boolean;
  timestamp: number;
}

export interface RequestUser {
  id: string;
  profileImageUrl: string;
  username: string;
}

export interface RequestItem {
  defaultImage: string;
  id: string;
  name: string;
}

export interface Requested {
  amount: number;
  text: number;
}
