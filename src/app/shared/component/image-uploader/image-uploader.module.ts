import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared-module/shared.module';

import { ImageUploaderComponent } from './image-uploader.component';
import { ImagePopoverComponent } from './partials/image-popover/image-popover.component';

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    ImageUploaderComponent,
    ImagePopoverComponent,
  ],
  entryComponents: [
    ImagePopoverComponent,
  ],
  exports: [
    ImageUploaderComponent,
  ],
})
export class ImageUploaderModule {
}
