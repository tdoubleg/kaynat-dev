import { NgModule } from '@angular/core';
import { GoogleMapsModule } from '@angular/google-maps';

import { SharedModule } from '../../shared-module/shared.module';
import { PlaceAutocompleteModule } from '../place-autocomplete/place-autocomplete.module';

import { FilterComponent } from './filter.component';

@NgModule({
  declarations: [FilterComponent],
  imports: [
    SharedModule,
    PlaceAutocompleteModule,
    GoogleMapsModule,
  ],
  exports: [
    FilterComponent,
  ],
})
export class FilterModule {
}
