import { Injectable } from '@angular/core';
import {
  Action,
  AngularFirestore,
  CollectionReference,
  DocumentChangeAction,
  DocumentData,
  DocumentSnapshot,
  Query,
  QueryDocumentSnapshot,
  QuerySnapshot,
} from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import * as geofire from 'geofire-common';
import { Observable } from 'rxjs/internal/Observable';
import { map, tap } from 'rxjs/operators';

import { ImageService } from '../../component/image-uploader/services/image.service';
import { User } from '../user/models/user';
import { UserService } from '../user/user.service';

import { LatLng, Product, ProductStatusEnum, Rating } from './models/product';
import { Unit } from './models/unit';

const THOUSAND: number = 1000;

@Injectable({
  providedIn: 'root',
})
export class ProductService {

  public units: Unit[];
  public selectedItem: Product;

  constructor(private formBuilder: FormBuilder,
              private translateService: TranslateService,
              private imageService: ImageService,
              private userService: UserService,
              private firestore: AngularFirestore) {
    this.getUnits();

  }

  public initForm(product?: Product): FormGroup {
    const minPrice: number = 0.01;

    return this.formBuilder.group({
      id: [product?.id ?? undefined],
      name: [product?.name ?? undefined, Validators.required],
      description: [product?.description ?? ''],
      images: [product?.images ?? []],
      location: [product?.location ?? undefined, Validators.required],
      rating: this.getRatingForm(product?.rating),
      status: [product?.status ?? ProductStatusEnum.PUBLISHED],
      timestamp: [product?.timestamp ?? new Date().getTime()],
      portion: this.formBuilder.group({
        unit: [product?.portion.unit ?? undefined, Validators.required],
        size: [product?.portion.size ?? undefined, [Validators.required, Validators.pattern('^[0-9]*$')]],
        price: [product?.portion.price ?? '', [Validators.required, Validators.min(minPrice)]],
      }),
    });
  }

  public getAllPublishedProducts(): Observable<Product[]> {
    return this.firestore.collection('products',
      (ref: CollectionReference) => ref.where('status', '==', ProductStatusEnum.PUBLISHED))
      .snapshotChanges()
      .pipe(
        map((documents: DocumentChangeAction<unknown>[]) =>
          documents.map((document: DocumentChangeAction<unknown>) => this.mapToProduct(document)),
        ),
      );
  }

  public getAllProductsForRange(center: LatLng, radius: number = 5): Promise<Product[]> {
    const _center: number[] = [center.lat, center.lng];
    const radiusInM: number = radius * THOUSAND;

    const bounds: string[][] = geofire.geohashQueryBounds(_center, radiusInM);
    const promises: Promise<QuerySnapshot<DocumentData>>[] = [];
    for (const b of bounds) {
      const query: Query = this.firestore.collection('products').ref
        .orderBy('location.hash')
        .startAt(b[0])
        .endAt(b[1]);

      promises.push(query.get());
    }

    // collect all the query results together into a single list

    return Promise.all(promises).then((snapshots: (QuerySnapshot<DocumentData>[])) => {
      const matchingDocs: Product[] = [];
      for (const snap of snapshots) {
        for (const doc of snap.docs) {
          const docData: Product = doc.data() as Product;
          const lat: number = docData.location.lat;
          const lng: number = docData.location.lng;

          // we have to filter out a few false positives due to GeoHash
          // accuracy, but most will match
          const distanceInKm: number = geofire.distanceBetween([lat, lng], _center);
          const distanceInM: number = distanceInKm * THOUSAND;
          if (distanceInM <= radiusInM) {
            matchingDocs.push(docData);
          }
        }
      }

      return matchingDocs;
    });
  }

  public getAllProductsOfUser(userId: string): Observable<Product[]> {
    return this.firestore.collection('products',
      (ref: CollectionReference) => ref.where('user.id', '==', userId))
      .snapshotChanges()
      .pipe(
        tap(() => console.debug('getAllProductsForUser$')),
        map((documents: DocumentChangeAction<unknown>[]) =>
          documents.map((document: DocumentChangeAction<unknown>) => this.mapToProduct(document)),
        ),
      );
  }

  public getFavoritesOfUser(userId: string): Observable<Product[]> {
    return this.firestore.doc(`user/${userId}`).collection('favorites')
      .snapshotChanges()
      .pipe(
        tap(() => console.debug('getFavoritesOfUser')),
        map((documents: DocumentChangeAction<unknown>[]) =>
          documents.map((document: DocumentChangeAction<unknown>) => this.mapToProduct(document)),
        ),
      );
  }

  public getProductById(itemId: string): Observable<Product> {
    return this.firestore.doc(`products/${itemId}`)
      .snapshotChanges()
      .pipe(
        map((document: Action<DocumentSnapshot<unknown>>) => {
          const data: Product = document.payload.data() as Product;
          const product: Product = {
            ...data,
            id: itemId,
          };

          return product;
        }),
      );
  }

  public async publishProduct(form: FormGroup): Promise<Product> {
    const user: Partial<User> = {
      id: this.userService.user$.value.id,
      username: this.userService.user$.value.username,
      profileImageUrl: this.userService.user$.value.profileImageUrl,
    };
    const { location, ...productValues } = form.value;

    const product: Product = {
      user,
      ...productValues,
      location: location.value,
    };

    try {
      product.id = product.id ? product.id : this.firestore.createId();
      if (this.imageService.images.length > 0) {
        product.images = await this.imageService.uploadImages(`products/${product.user.id}/${product.id}`);
      }

      await this.firestore.doc(`products/${product.id}`).set({ ...product }, { merge: true });
      await this.firestore.doc(`user/${user.id}`).collection('products').doc(product.id).set({ ...product },
        { merge: true });

      return Promise.resolve(product);
    } catch (e) {
      console.error('error while publishing', e);

      return Promise.reject(e);
    }
  }

  public async setProductStatus(product: Product, status: ProductStatusEnum): Promise<void> {
    // TODO: Create a collection for deleted products as backup
    if (status === ProductStatusEnum.DELETED) {
      await this.firestore.doc(`products/${product.id}`).delete();
      await this.firestore.doc(`deleted_products/${product.id}`).set({ ...product });

      return;
    }

    await this.firestore.doc(`products/${product.id}`).update({ status });

    return;
  }

  private getUnits(): void {
    this.units = [
      {
        value: 0,
        label: this.translateService.instant('unit.g.long'),
      },
      {
        value: 1,
        label: this.translateService.instant('unit.kg.long'),
      },
      {
        value: 2,
        label: this.translateService.instant('unit.l.long'),
      },
      {
        value: 3,
        label: this.translateService.instant('unit.ml.long'),
      },
      {
        value: 4,
        label: this.translateService.instant('unit.pc.long'),
      },
    ];
  }

  private getRatingForm(rating: Rating): FormGroup {
    return this.formBuilder.group({
      average: [rating ? rating.average : 0],
      rates: [rating ? rating.rates : 0],
    });
  }

  private mapToProduct(document: DocumentChangeAction<unknown>): Product {
    const doc: QueryDocumentSnapshot<unknown> = document.payload.doc;
    const data: Product = doc.data() as Product;

    return {
      ...data,
      id: doc.id,
    };
  }
}
