import { NgModule } from '@angular/core';

import { ImageUploaderModule } from '../../shared/component/image-uploader/image-uploader.module';
import { SharedModule } from '../../shared/shared-module/shared.module';

import { LoginPageRoutingModule } from './login-routing.module';
import { LoginPage } from './login.page';
import { LoginFormComponent } from './partials/login-form/login-form.component';
import { RegisterFormComponent } from './partials/register-form/register-form.component';

@NgModule({
  imports: [
    LoginPageRoutingModule,
    SharedModule,
    ImageUploaderModule,
  ],
  declarations: [
    LoginPage,
    LoginFormComponent,
    RegisterFormComponent,
  ],
})
export class LoginPageModule {
}
