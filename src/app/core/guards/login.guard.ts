import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { NavController } from '@ionic/angular';
import firebase from 'firebase';
import { NgxFirebaseAuthService } from 'ngx-firebase-auth';
import { take } from 'rxjs/operators';

import { UserService } from '../../shared/services/user/user.service';
import FirebaseUser = firebase.User;

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {
  constructor(private navCtrl: NavController,
              private userService: UserService,
              private authService: NgxFirebaseAuthService) {
  }

  public async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const user: FirebaseUser = await this.authService.currentUser$.pipe(take(1)).toPromise();
    if (user) {
      await this.navCtrl.navigateForward(['/tabs/home']);

      return true;
    }
    if (state.url.indexOf('login') > -1) {
      return true;
    }

    await this.navCtrl.navigateForward(['/login']);

  }
}
