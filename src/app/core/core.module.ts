import { NgModule } from '@angular/core';

import { AuthenticationGuard } from './guards/authentication.guard';
import { LoginGuard } from './guards/login.guard';
import { WelcomeGuard } from './guards/welcome.guard';

@NgModule({
  providers: [
    AuthenticationGuard,
    LoginGuard,
    WelcomeGuard,
  ],
})
export class CoreModule {
}
