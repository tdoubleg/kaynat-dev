import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
import firebase from 'firebase';
import { NgxFirebaseAuthService } from 'ngx-firebase-auth';

import { LocaleService } from './shared/services/locale/locale.service';
import { UserService } from './shared/services/user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private platform: Platform,
              private splashScreen: SplashScreen,
              private statusBar: StatusBar,
              private localeService: LocaleService,
              private userService: UserService,
              private authService: NgxFirebaseAuthService) {
    this.initializeApp();
  }

  public initializeApp(): void {
    this.localeService.getUsersLocale();

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.loadUser();
      this.setDarkMode();
    });
  }

  private loadUser(): void {
    this.authService.currentUser$.subscribe((user: firebase.User) => {
      console.debug('AuthService currentUser$', user);
      if (user) {
        this.userService.getUserInformation(user.uid);
      }
    });
  }

  private setDarkMode(): void {
    let darkModeValue: boolean = JSON.parse(localStorage.getItem('darkMode'));
    if (darkModeValue === null) {
      const prefersDark: MediaQueryList = window.matchMedia('(prefers-color-scheme: dark)');
      darkModeValue = prefersDark.matches;
      localStorage.setItem('darkMode', String(darkModeValue));
    }
    document.body.classList.toggle('dark', darkModeValue);
  }
}
