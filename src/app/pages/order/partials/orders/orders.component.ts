import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { takeUntil } from 'rxjs/operators';

import { GeneralService } from '../../../../shared/services/general/general.service';
import { ChatService } from '../../../chat/services/chat.service';
import { Request } from '../../../detail/service/request/models/request';
import { RequestService } from '../../../detail/service/request/request.service';
import { BaseRequestComponent } from '../../base/base-request.component';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent extends BaseRequestComponent {
  constructor(protected requestService: RequestService,
              protected chatService: ChatService,
              protected navCtrl: NavController,
              protected generalService: GeneralService) {
    super(requestService, chatService, navCtrl, generalService);
  }

  public getItems(): void {
    this.requestService.ownRequests$.pipe(
      takeUntil(this.destroy$),
    ).subscribe((requests: Request[]) => {
      this.requests = requests;
      this.filterRequests();
    });
  }
}
