import { AfterViewChecked, Component } from '@angular/core';
import { IonItemSliding, NavController } from '@ionic/angular';
import { takeUntil } from 'rxjs/operators';

import { GeneralService } from '../../../../shared/services/general/general.service';
import { ChatService } from '../../../chat/services/chat.service';
import { Request, RequestStatusEnum } from '../../../detail/service/request/models/request';
import { RequestService } from '../../../detail/service/request/request.service';
import { BaseRequestComponent } from '../../base/base-request.component';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss'],
})
export class RequestsComponent extends BaseRequestComponent implements AfterViewChecked {

  constructor(protected requestService: RequestService,
              protected chatService: ChatService,
              protected navCtrl: NavController,
              protected generalService: GeneralService) {
    super(requestService, chatService, navCtrl, generalService);
  }

  public ngAfterViewChecked(): void {
    const firstSlider: IonItemSliding = this.slider.first;
    const showedRequestSlider: boolean = JSON.parse(localStorage.getItem('showedRequestSlider'));
    if (firstSlider && !showedRequestSlider) {
      localStorage.setItem('showedRequestSlider', JSON.stringify(true));
      setTimeout(async () => {
        await firstSlider.open('end');
      }, 500);
      setTimeout(async () => {
        await firstSlider.close();
      }, 2000);

    }
  }

  public getItems(): void {
    this.requestService.userRequests$.pipe(
      takeUntil(this.destroy$),
    ).subscribe((requests: Request[]) => {
      this.requests = requests;
      this.filterRequests();
    });
  }

  public async setStatus(request: Request, status: RequestStatusEnum): Promise<void> {
    if (status === RequestStatusEnum.ACCEPTED) {
      await this.generalService.presentConfirmAlertWithHandler(
        'request.accept.header',
        'request.accept.message',
        'request.accept.button',
        () => {
          console.debug('accepted');
          this.requestService.updateRequestStatus(request.id, status);
        });
    } else if (status === RequestStatusEnum.DECLINED) {
      await this.generalService.presentConfirmAlertWithHandler(
        'request.decline.header',
        'request.decline.message',
        'request.decline.button',
        () => {
          this.requestService.updateRequestStatus(request.id, status);
        });
    }
  }

}
