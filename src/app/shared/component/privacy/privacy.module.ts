import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared-module/shared.module';

import { PrivacyComponent } from './privacy.component';

@NgModule({
  declarations: [
    PrivacyComponent,
  ],
  imports: [
    SharedModule,
  ],
  exports: [
    PrivacyComponent,
  ],
})
export class PrivacyModule {
}
