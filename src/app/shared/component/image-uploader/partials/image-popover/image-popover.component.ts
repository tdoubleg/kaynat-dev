import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-image-popover',
  templateUrl: './image-popover.component.html',
  styleUrls: ['./image-popover.component.scss'],
})
export class ImagePopoverComponent {
  public sliderOptions: unknown;
  public fileURL: SafeResourceUrl;

  private index: number;

  constructor(private navParams: NavParams,
              private sanitizer: DomSanitizer,
              private modalCtrl: ModalController) {
    this.sliderOptions = {
      zoom: {
        maxRatio: 1,
        slidesPerView: 1,
      },
    };
    this.fileURL = this.navParams.data.url;
    this.index = this.navParams.data.index;
  }

  public close(): void {
    this.modalCtrl.dismiss({ shouldDelete: false, index: this.index });
  }

  public deleteImage(): void {
    this.modalCtrl.dismiss({ shouldDelete: true, index: this.index });
  }
}
