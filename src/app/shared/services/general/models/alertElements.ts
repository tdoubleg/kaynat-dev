//tslint:disable
import { AlertButton, AlertInput } from 'node_modules/@ionic/core/dist/types/components/alert/alert-interface';

export  interface IAlertButton extends AlertButton {
}

export interface IAlertInput extends AlertInput {
}
