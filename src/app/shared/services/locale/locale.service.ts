import { registerLocaleData } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import localeDe from '@angular/common/locales/de';
import localEn from '@angular/common/locales/en';
import localeTr from '@angular/common/locales/tr';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root',
})
export class LocaleService {

  private _locale: string;
  private lang$: Subscription;

  constructor(private translateService: TranslateService,
              private httpClient: HttpClient) {
  }

  public set locale(value: string) {
    this._locale = value;
  }

  public get locale(): string {
    return this._locale || 'en';
  }

  public getUsersLocale(): void {
    // tslint:disable-next-line:no-any
    const navigator: any = window.navigator;

    let lang: string = navigator.language || 'en';

    if (lang.includes('-')) {
      const dashIndex: number = lang.indexOf('-');
      lang = lang.substr(0, dashIndex);
    }

    this.lang$ = this.httpClient.get(`./assets/i18n/${lang}.json`).subscribe(
      () => this.registerCulture(lang),
      () => this.registerCulture('de'),
    );

  }

  private registerCulture(lang: string): void {
    this.translateService.setDefaultLang(lang);
    if (!lang) {
      return;
    }
    this.locale = lang;

    // register locale data since only the en-US locale data comes with Angular
    switch (lang) {
      case 'de': {
        registerLocaleData(localeDe);
        break;
      }
      case 'tr': {
        registerLocaleData(localeTr);
        break;
      }
      default: {
        registerLocaleData(localEn);
      }
    }
  }
}
