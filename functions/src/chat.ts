import { firestore, messaging } from 'firebase-admin';
import { Change } from 'firebase-functions';

import { Chat, Message } from './models/chat';
import { Request } from './models/request';
import { notify } from './notification';
import Firestore = firestore.Firestore;
import QueryDocumentSnapshot = firestore.QueryDocumentSnapshot;
import MessagingPayload = messaging.MessagingPayload;

export function createChat(db: Firestore, snapshot: QueryDocumentSnapshot): Promise<FirebaseFirestore.WriteResult | void> {
  const data: Request = snapshot.data() as Request;

  return db.doc(`chats/${snapshot.id}`)
    .set({
      createdAt: data.timestamp,
      createdBy: { ...data.fromUser },
      owner: data.ownerId,
      item: { ...data.item },
      messages: [
        {
          content: data.requested.text,
          createdAt: data.timestamp,
          user: data.fromUser,
        },
      ],
    })
    .catch((error: unknown) => {
      console.error('error while creating chat', error);
    });
}

export function newMessage(snapshot: Change<QueryDocumentSnapshot>): void {
  const data: Chat = snapshot.after.data() as Chat;
  if (data) {
    const messages: Message[] = data.messages;
    const createdByUserId: string = data.createdBy.id;
    const ownerUserId: string = data.owner;

    if (messages.length > 0) {
      const lastMessage: Message = messages[messages.length - 1];

      const payload: MessagingPayload = {
        notification: {
          title: 'Du hast eine neue Nachricht',
          body: (lastMessage.content.length <= 100) ?
            lastMessage.content : lastMessage.content.substring(0, 97) + '...',
        },
        data: {
          itemId: data.item.id,
          chatId: snapshot.after.id,
        },
      };

      if (lastMessage.user.id === createdByUserId) {
        // send message to item owner
        notify(ownerUserId, payload);
      } else {
        // send message to chat creator
        notify(createdByUserId, payload);
      }
    }
  }
}
