import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NavController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

import { FavoriteService } from '../../../../shared/services/favorite/favorite.service';
import { Product } from '../../../../shared/services/product/models/product';
import { ProductService } from '../../../../shared/services/product/product.service';
import { User } from '../../../../shared/services/user/models/user';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
})
export class FavoritesComponent implements OnChanges {
  @Input() public user: User;

  public items$: BehaviorSubject<Partial<Product>[] | Product[]>;

  public constructor(private productService: ProductService,
                     private favoriteService: FavoriteService,
                     private navCtrl: NavController) {
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (this.user) {
      this.getItems();
    }
  }

  public getItems(): void {
    this.items$ = this.favoriteService.favoriteItems;
  }

  public async updateFavorite(event: Event, item: Partial<Product>): Promise<void> {
    event.preventDefault();
    await this.favoriteService.updateFavorite(item);
  }

  public async navigate(itemId: string): Promise<void> {
    await this.navCtrl.navigateForward(`tabs/detail/${itemId}`);
  }
}
