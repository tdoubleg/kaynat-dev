import { NgModule } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { NgxCurrencyModule } from 'ngx-currency';

import { ImageUploaderModule } from '../../shared/component/image-uploader/image-uploader.module';
import { SharedModule } from '../../shared/shared-module/shared.module';
import { HomePageModule } from '../home/home.module';

import { CreatePageRoutingModule } from './create-routing.module';
import { CreatePage } from './create.page';

@NgModule({
  imports: [
    CreatePageRoutingModule,
    ImageUploaderModule,
    NgxCurrencyModule,
    SharedModule,
    HomePageModule,
  ],
  providers: [
    NavParams,
  ],
  declarations: [
    CreatePage,
  ],
})
export class CreatePageModule {
}
