// this file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// the list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  geocodeApi: 'https://photon.komoot.io/api/?q=',
  firebaseConfig: {
    apiKey: 'AIzaSyBGZI1wQ24v7tZzL6NQO5dnoIm6NyyMTsU',
    authDomain: 'kaynat-dev.firebaseapp.com',
    databaseURL: 'https://kaynat-dev.firebaseio.com',
    projectId: 'kaynat-dev',
    storageBucket: 'kaynat-dev.appspot.com',
    messagingSenderId: '413453986605',
    appId: '1:413453986605:web:d0f973bdbfb493147b41a4',
    measurementId: 'G-ZYYGVR75KE',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
