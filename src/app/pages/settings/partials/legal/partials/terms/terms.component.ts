import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss'],
})
export class TermsComponent {

  constructor(private modalCtrl: ModalController) { }

  public close(): void {
    this.modalCtrl.dismiss();
  }
}
