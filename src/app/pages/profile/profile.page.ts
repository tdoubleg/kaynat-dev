import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgxFirebaseAuthService } from 'ngx-firebase-auth';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

import { User } from '../../shared/services/user/models/user';
import { UserService } from '../../shared/services/user/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {

  public user: User;
  public currentState: string;
  private destroy$: Subject<void>;

  constructor(private authService: NgxFirebaseAuthService,
              private userService: UserService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.destroy$ = new Subject<void>();
  }

  public ngOnInit(): void {
    this.currentState = 'account';

    this.userService.user$
      .pipe(takeUntil(this.destroy$))
      .subscribe((user: User) => {
        this.user = user;
      });

    this.activatedRoute.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe((params: Params) => {
        this.currentState = params.tab ?? 'account';
      });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public segmentChanged(segment: CustomEvent): void {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: { tab: segment.detail.value },
      });
  }
}
