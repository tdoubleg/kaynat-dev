import { Injectable } from '@angular/core';
import { GeolocationPosition, Plugins } from '@capacitor/core';

const { Geolocation } = Plugins;

@Injectable({
  providedIn: 'root',
})
export class GeolocationService {

  public async getCurrentPosition(): Promise<GeolocationPosition> {
    return Geolocation.getCurrentPosition();
  }
}
