import { Capacitor, Plugins } from '@capacitor/core';

import { environment } from '../../../../../environments/environment';

const { FirebaseAnalytics, FirebaseCrashlytics } = Plugins;

export class PrivacyService {
  
  /**
   * Platform: Web
   * Configure and initialize the firebase app.
   * @param options - firebase web app configuration options
   * */
  
  public static initFirebaseForWeb(): void {
    if (Capacitor.getPlatform() === 'Web') {
      FirebaseAnalytics.initializeFirebase(environment.firebaseConfig);
    }
  }
  
  /**
   * Platform: Android/iOS
   * Enables/disables automatic data collection by Crashlytics.
   * @params enabled - true/false to enable/disable reporting
   * @returns none
   */
  public static enableAutomaticDataCollectionByCrashlytics(enabled: boolean): void {
    if (Capacitor.isNative) {
      FirebaseCrashlytics.setEnabled({
        enabled,
      });
    }
  }
  
  /**
   * Platform: iOS
   * Indicates whether or not automatic data collection is enabled
   * @params enabled - true/false to enable/disable reporting
   * @returns none
   */
  public static isCrashlyticsEnabled(): boolean {
    if (Capacitor.isNative) {
      return FirebaseCrashlytics.isEnabled();
    }
  }
  
  /**
   * Platform: Web/Android/iOS
   * Sets whether analytics collection is enabled for this app on this device.
   * @param name - enabled - boolean true/false
   * @returns void
   */
  public static enableAnalyticsCollection(enabled: boolean): void {
    if (Capacitor.isNative) {
      FirebaseAnalytics.setCollectionEnabled({
        enabled,
      });
    }
  }
  
  /**
   * Platform: Web/Android/iOS
   * Sets whether personalized ads are enabled or disabled.
   * @param name - enabled - boolean true/false
   * @returns void
   */
  public static personalizedAds(enabled: boolean): void {
    if (Capacitor.isNative) {
      FirebaseAnalytics.setUserProperty({
        name: 'allow_personalized_ads',
        value: enabled,
      });
    }
  }
  
  /**
   * Platform: Android/iOS
   * Sets the current screen name, which specifies the current visual context in your app.
   * @param screenName - name of the current screen to track
   *        nameOverride - name of the screen class to override
   * @returns instanceId - individual instance id value
   * https://firebase.google.com/docs/analytics/screenviews
   */
  public static setScreenName(screenName: string, nameOverride: string): void {
    if (Capacitor.isNative) {
      FirebaseAnalytics.setScreenName({
        screenName,
        nameOverride,
      });
    }
  }
  
  /**
   * Platform: Web/Android/iOS
   * Logs an app event.
   * @param name - name of the event to log
   *        params - key/value pairs of properties (25 maximum per event)
   * @returns void
   */
  public static logEvent(eventName: string, params: { [key: string]: string | boolean }): void {
    if (Capacitor.isNative) {
      FirebaseAnalytics.logEvent({
        name: eventName,
        params,
      });
    }
  }
}
