import * as admin from 'firebase-admin';
import { firestore, messaging } from 'firebase-admin';

import { FCM } from './models/fcm';
import DocumentData = firestore.DocumentData;
import DocumentSnapshot = firestore.DocumentSnapshot;
import MessagingDevicesResponse = messaging.MessagingDevicesResponse;
import MessagingPayload = messaging.MessagingPayload;

export function notify(userId: string, payload: MessagingPayload): Promise<MessagingDevicesResponse | null> | null {
  return admin.firestore().doc(`users/${userId}`)
    .get()
    .then((res: DocumentSnapshot<DocumentData>) => {
      const fcmInfo: FCM = res.get('fcm');
      if (fcmInfo) {
        return admin.messaging().sendToDevice(fcmInfo.token, payload);
      }

      return null;
    });

}
