import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SharedModule } from '../../shared/shared-module/shared.module';

import { OrderPageRoutingModule } from './order-routing.module';
import { OrderPage } from './order.page';
import { OrdersComponent } from './partials/orders/orders.component';
import { RequestsComponent } from './partials/requests/requests.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderPageRoutingModule,
    SharedModule,
  ],
  declarations: [OrderPage, RequestsComponent, OrdersComponent],
})
export class OrderPageModule {}
