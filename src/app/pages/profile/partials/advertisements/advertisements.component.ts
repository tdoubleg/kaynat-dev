import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs/internal/Observable';

import { Product, ProductStatusEnum } from '../../../../shared/services/product/models/product';
import { ProductService } from '../../../../shared/services/product/product.service';
import { User } from '../../../../shared/services/user/models/user';
import { CreatePage } from '../../../create/create.page';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-advertisements',
  templateUrl: './advertisements.component.html',
  styleUrls: ['./advertisements.component.scss'],
})
export class AdvertisementsComponent implements OnChanges {
  @Input() public user: User;

  public items$: Observable<Product[]>;
  // tslint:disable-next-line:typedef
  public ProductStatusEnum = ProductStatusEnum;

  constructor(private productService: ProductService,
              private modalCtrl: ModalController,
              private routerOutlet: IonRouterOutlet) {
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (this.user) {
      this.getItems();
    }
  }

  public getItems(): void {
    this.items$ = this.productService.getAllProductsOfUser(this.user.id).pipe();
  }

  public async editItem(item: Product): Promise<void> {
    const modal: HTMLIonModalElement = await this.modalCtrl.create({
      component: CreatePage,
      componentProps: { item, editing: true },
      cssClass: 'edit-modal',
      swipeToClose: true,
      backdropDismiss: true,
      presentingElement: this.routerOutlet.nativeEl,
    });

    return await modal.present();
  }

  public async setProductStatus(product: Product, status: ProductStatusEnum): Promise<void> {
    await this.productService.setProductStatus(product, status);
  }
}
