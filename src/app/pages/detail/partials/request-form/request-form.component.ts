import { Component, Input } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { ModalController } from '@ionic/angular';
import * as moment from 'moment-timezone';

import { GeneralService } from '../../../../shared/services/general/general.service';
import { Product } from '../../../../shared/services/product/models/product';
import { User } from '../../../../shared/services/user/models/user';
import { UserService } from '../../../../shared/services/user/user.service';
import { Request, RequestStatusEnum } from '../../service/request/models/request';
import { RequestService } from '../../service/request/request.service';

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.scss'],
})
export class RequestFormComponent {

  @Input() public item: Product;
  @Input() public portionAmount: number;

  public requestText: string;

  constructor(private modalCtrl: ModalController,
              private userService: UserService,
              private generalService: GeneralService,
              private requestService: RequestService) {
  }

  public get isNotAndroid(): boolean {
    return Capacitor.getPlatform() !== 'android';
  }

  public async sendRequest(): Promise<void> {
    const loader: HTMLIonLoadingElement = await this.generalService.presentLoading('request.loading.message');
    try {
      const user: User = this.userService.user$.value;

      const newRequest: Request = {
        status: RequestStatusEnum.ACCEPTED,
        timestamp: moment().unix(),
        ownerId: this.item.user.id,
        read: false,
        requested: {
          amount: this.portionAmount,
          text: this.requestText,
        },
        item: {
          id: this.item.id,
          name: this.item.name,
          defaultImage: this.item.images[0],
        },
        fromUser: {
          id: user.id,
          username: user.username,
          profileImageUrl: user.profileImageUrl,
        },
      };
      await this.requestService.setRequest(newRequest);
      await this.close();
      await this.generalService.presentInfoAlert('request.success.header', 'request.success.message');
    } catch (e) {
      console.error(e);
      await this.generalService.presentInfoAlert('request.error.header', 'request.error.message');
    } finally {
      await loader.dismiss();
    }
  }

  public async close(): Promise<void> {
    await this.modalCtrl.dismiss();
  }
}
