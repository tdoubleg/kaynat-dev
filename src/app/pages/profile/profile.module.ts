import { NgModule } from '@angular/core';

import { ImageUploaderModule } from '../../shared/component/image-uploader/image-uploader.module';
import { ItemViewModule } from '../../shared/component/item-view/item-view.module';
import { UserProfileModule } from '../../shared/component/user-profile/user-profile.module';
import { SharedModule } from '../../shared/shared-module/shared.module';

import { AccountComponent } from './partials/account/account.component';
import { AdvertisementsComponent } from './partials/advertisements/advertisements.component';
import { FavoritesComponent } from './partials/favorites/favorites.component';
import { ProfilePageRoutingModule } from './profile-routing.module';
import { ProfilePage } from './profile.page';

@NgModule({
  imports: [
    SharedModule,
    ProfilePageRoutingModule,
    ImageUploaderModule,
    ItemViewModule,
    UserProfileModule,
  ],
  declarations: [
    ProfilePage,
    AccountComponent,
    AdvertisementsComponent,
    FavoritesComponent,
  ],
})
export class ProfilePageModule {
}
