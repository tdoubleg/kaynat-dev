import { Component } from '@angular/core';
import { filter, switchMap, tap } from 'rxjs/operators';

import { RequestService } from '../pages/detail/service/request/request.service';
import { UserService } from '../shared/services/user/user.service';

import { TabPage } from './model/tabPage';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
})
export class TabsPage {
  public appPages: TabPage[];

  constructor(private requestService: RequestService,
              private userService: UserService) {
    this.appPages = [
      {
        title: 'home',
        url: 'home',
        icon: 'map-sharp',
      },
      {
        title: 'create',
        url: 'create',
        icon: 'add-sharp',
      },
      {
        title: 'orders',
        url: 'orders',
        icon: 'basket-sharp',
        badgeCount: 0,
      },
      {
        title: 'profile',
        url: 'profile',
        icon: 'person-sharp',
      },
      {
        title: 'settings',
        url: 'settings',
        icon: 'cog-sharp',
      },
    ];

    this.getUserRequests();
  }

  private getUserRequests(): void {
    this.userService.userId.pipe(
      filter((userId: string) => userId !== null),
      switchMap((userId: string) => this.requestService.getUserRequests(userId)),
      switchMap(() => this.requestService.numberOfUnreadRequests$),
      tap((numberOfUnreadRequests: number) => {
        const page: TabPage = this.appPages.find((pages: TabPage) => pages.title === 'orders');
        page.badgeCount = numberOfUnreadRequests;
      }),
    ).subscribe();
  }
}
