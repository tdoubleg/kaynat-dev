import { Product } from '../../../../../shared/services/product/models/product';
import { BaseUser } from '../../../../../shared/services/user/models/user';

export interface BaseRequest {
  ownRequests: Request[];
  userRequests: Request[];
}

export interface Request {
  id?: string;
  item: Partial<Product>;
  requested: Requested;
  timestamp: number;
  fromUser: BaseUser;
  status: RequestStatusEnum;
  ownerId: string;
  read: boolean;
}

export interface Requested {
  amount: number;
  text: string;
}

export enum RequestStatusEnum {
  DECLINED,
  ACCEPTED,
  PENDING,
}

export interface Message {
  id: string;
  user: BaseUser;
  content: string;
  createdAt: number;
}
