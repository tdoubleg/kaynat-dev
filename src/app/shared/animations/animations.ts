//tslint:disable
import { AnimationController } from '@ionic/angular';
import { Animation } from '@ionic/core';

export class Animations {
  constructor(private animationCtrl: AnimationController) {
  }

  public slideInOut(close?: boolean, duration: number = 250): Animation {
    const slideAnimation: Animation = this.animationCtrl.create()
      .addElement(document.querySelector('.form-wrapper'))
      .duration(duration)
      .iterations(1);

    if (close) {
      slideAnimation.fromTo('transform', 'translateY(0px)', 'translateY(500px)');
    } else {
      slideAnimation.fromTo('transform', 'translateY(500px)', 'translateY(0px)');
    }

    return slideAnimation;
  }
}
