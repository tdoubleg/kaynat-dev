import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  CollectionReference,
  DocumentChangeAction,
  DocumentData,
  DocumentReference,
  QueryDocumentSnapshot,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { map, tap } from 'rxjs/operators';

import { BaseRequest, Request, RequestStatusEnum } from './models/request';

@Injectable({
  providedIn: 'root',
})
export class RequestService {

  public requests$: BehaviorSubject<BaseRequest>;
  public userRequests$: BehaviorSubject<Request[]>;
  public ownRequests$: BehaviorSubject<Request[]>;
  public numberOfUnreadRequests$: BehaviorSubject<number>;

  constructor(private firestore: AngularFirestore) {
    this.requests$ = new BehaviorSubject<BaseRequest>(null);
    this.userRequests$ = new BehaviorSubject<Request[]>(null);
    this.ownRequests$ = new BehaviorSubject<Request[]>(null);
    this.numberOfUnreadRequests$ = new BehaviorSubject<number>(0);
  }

  public setRequest(request: Request): Promise<DocumentReference<unknown>> {
    return this.firestore.collection('requests').add({ ...request });
  }

  public updateRequestReadStatus(requestId: string): Promise<void> {
    return this.firestore.doc(`requests/${requestId}`).update({
      read: true,
    });
  }

  public async updateRequestStatus(requestId: string, status: RequestStatusEnum): Promise<void> {
    try {
      await this.firestore.doc(`requests/${requestId}`).update({ status });
      await this.updateRequestReadStatus(requestId);
    } catch (e) {
      console.error('error while updating');
    }
  }

  public getUserRequests(userId: string): Observable<BaseRequest> {
    const userRequests$ = this.firestore.collection('requests',
      (ref: CollectionReference<DocumentData>) => ref.where('ownerId', '==', userId)).snapshotChanges();
    const ownRequests$ = this.firestore.collection('requests',
      (ref: CollectionReference<DocumentData>) => ref.where('fromUser.id', '==', userId)).snapshotChanges();

    return combineLatest([userRequests$, ownRequests$]).pipe(
      map((requests: [(DocumentChangeAction<unknown>)[], (DocumentChangeAction<unknown>)[]]) =>
        (requests.map((request: (DocumentChangeAction<unknown>[])) => this.mapResponseToRequest(request))),
      ),
      map((mappedRequests: Request[][]) =>
        ({ userRequests: mappedRequests[0], ownRequests: mappedRequests[1] }),
      ),
      tap((request: BaseRequest) => {
        const numberOfUnreadRequests: number = request.userRequests.filter((req: Request) => !req.read).length;
        this.numberOfUnreadRequests$.next(numberOfUnreadRequests);
        this.userRequests$.next(request.userRequests);
        this.ownRequests$.next(request.ownRequests);
      }),
    );

  }

  private mapResponseToRequest(documents: (DocumentChangeAction<unknown>[])): Request[] {
    return documents.map((document: DocumentChangeAction<unknown>) => {
      const doc: QueryDocumentSnapshot<unknown> = document.payload.doc;
      const data: unknown = doc.data();

      return {
        ...data as Request,
        id: doc.id,
      };
    });
  }
}
