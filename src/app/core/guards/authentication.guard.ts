import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { NavController } from '@ionic/angular';
import firebase from 'firebase';
import { NgxFirebaseAuthService } from 'ngx-firebase-auth';
import { take } from 'rxjs/operators';
import FirebaseUser = firebase.User;

@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(private navCtrl: NavController,
              private authService: NgxFirebaseAuthService) {
  }

  public async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    if (this.authService.authenticated) {
      return true;
    }
    const user: FirebaseUser = await this.authService.currentUser$.pipe(take(1)).toPromise();
    const path: string = next.routeConfig.path;
    const id: string = next.firstChild.params.id;

    if (path.indexOf('profile') > -1) {
      if (user && user.uid === id || user && !id) {
        return true;
      }

      if (!user) {
        await this.navCtrl.navigateForward(['/login'], { queryParams: { returnUrl: state.url } });
      } else {
        await this.navCtrl.navigateForward(['/']);
      }

      return false;
    }

    if (!user) {
      await this.navCtrl.navigateForward(['/login'], { queryParams: { returnUrl: state.url } });

      return false;
    }

    return true;
  }
}
