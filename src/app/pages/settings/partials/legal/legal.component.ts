import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { ImprintComponent } from './partials/imprint/imprint.component';
import { PrivacyPolicyComponent } from './partials/privacy-policy/privacy-policy.component';
import { TermsComponent } from './partials/terms/terms.component';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss'],
})
export class LegalComponent {

  constructor(private modalCtrl: ModalController) {
  }

  public async open(type: string): Promise<void> {
    const modal: HTMLIonModalElement = await this.modalCtrl.create({
      component: type === 'privacy' ? PrivacyPolicyComponent : type === 'legal' ? ImprintComponent : TermsComponent,
    });

    await modal.present();
  }
}
