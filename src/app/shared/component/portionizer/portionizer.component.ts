import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Product, ProductUnitEnum } from '../../services/product/models/product';

@Component({
  selector: 'app-portionizer',
  templateUrl: './portionizer.component.html',
  styleUrls: ['./portionizer.component.scss'],
})
export class PortionizerComponent {

  public unitEnum: typeof ProductUnitEnum;

  @Input() public item: Product;
  @Input() public amount: number;
  @Output() public amountChange: EventEmitter<number>;

  constructor() {
    this.unitEnum = ProductUnitEnum;
    this.amountChange = new EventEmitter<number>();
  }

  public increase(): void {
    this.amount += 1;
    this.amountChange.next(this.amount);
  }

  public decrease(): void {
    if (this.amount <= 1) {
      this.amount = 1;
    } else {
      this.amount -= 1;
    }
    this.amountChange.next(this.amount);
  }
}
