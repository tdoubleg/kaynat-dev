import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { IonContent, ModalController, NavParams } from '@ionic/angular';
import * as geofire from 'geofire-common';

import { GeneralService } from '../../shared/services/general/general.service';
import { Unit } from '../../shared/services/product/models/unit';
import { ProductService } from '../../shared/services/product/product.service';
import GeocoderResult = google.maps.GeocoderResult;

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  public productForm: FormGroup;
  public units: Unit[];
  public isEditing: boolean;
  public selectedLocation: GeocoderResult;

  @ViewChild(IonContent) private content: IonContent;

  constructor(private productService: ProductService,
              private router: Router,
              private navParams: NavParams,
              private modalCtrl: ModalController,
              private generalService: GeneralService) {
  }

  public ngOnInit(): void {
    this.isEditing = this.navParams.data.editing;
    this.productForm = this.productService.initForm(this.navParams.data.item);
    this.units = this.productService.units;
  }

  public async publish(): Promise<void> {
    const spinner: HTMLIonLoadingElement = await this.generalService.presentLoading('publish.loadingMessage');
    try {
      await this.productService.publishProduct(this.productForm);
      await this.generalService.presentConfirmAlertWithHandler(
        'publish.alert.success.header',
        'publish.alert.success.message',
        null,
        async () => {
          if (this.isEditing) {
            await this.modalCtrl.dismiss();
          } else {
            await this.router.navigateByUrl('/');
          }
        });
    } catch (e) {
      console.error('Not published', e);
      await this.generalService.presentInfoAlert('publish.alert.error.header', 'publish.alert.error.message', e);
    } finally {
      await spinner.dismiss();
    }
  }

  public convertLocationToForm(location: GeocoderResult): void {
    const lat: number = location?.geometry.location.lat();
    const lng: number = location?.geometry.location.lng();

    this.productForm.get('location').setValue(new FormGroup({
      value: new FormControl(location?.formatted_address),
      hash: new FormControl(lat && lng ? geofire.geohashForLocation([lat, lng]) : undefined),
      lat: new FormControl(lat),
      lng: new FormControl(location?.geometry.location.lng()),
    }));
  }

  public async close(): Promise<void> {
    await this.modalCtrl.dismiss();
  }

  public async scrollTo(): Promise<void> {
    if (Capacitor.getPlatform() === 'ios') {
      const keyboardOffset: number = 20;
      const yOffset: number = document.getElementById('product-price').offsetTop;
      await this.content.scrollToPoint(0, yOffset + keyboardOffset);
    }
  }
}
