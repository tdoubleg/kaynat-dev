import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

import { PrivacyService } from '../../shared/component/privacy/service/privacy.service';
import { Product } from '../../shared/services/product/models/product';
import { ProductService } from '../../shared/services/product/product.service';
import { User } from '../../shared/services/user/models/user';

@Component({
  selector: 'app-customer-profile',
  templateUrl: './customer-profile.page.html',
  styleUrls: ['./customer-profile.page.scss'],
})
export class CustomerProfilePage {

  public sortedProducts: Product[];
  public sortBy: string;
  public direction: string;
  public user: Partial<User>;

  private products: Product[];
  private destroy$: Subject<void>;

  constructor(private activatedRoute: ActivatedRoute,
              private productService: ProductService,
              private navCtrl: NavController) {
    this.direction = 'desc';
    this.sortBy = 'rating';
    this.destroy$ = new Subject<void>();
  }

  public ionViewDidEnter(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe((params: Params) => {
        const userId: string = params.userId;
        if (userId) {
          this.productService.getAllProductsOfUser(userId)
            .pipe(
              takeUntil(this.destroy$),
            )
            .subscribe((products: Product[]) => {
              this.user = products[0].user;
              this.products = products;
              this.sortItems();
            });
        }
      });

    PrivacyService.setScreenName('buyable', 'BuyablePage');
  }

  public ionViewDidLeave(): void {
    this.destroy$.next();
  }

  public showProductDetail(item: Product): void {
    this.productService.selectedItem = item;
    this.navCtrl.navigateForward(`/tabs/detail/${item.id}`).then();
  }

  public sortItems(): void {
    this.sortedProducts = [...this.products].sort((itemA: Product, itemB: Product) => {
      if (this.direction === 'asc') {
        switch (this.sortBy) {
          case 'rating':
            return itemA.rating.average < itemB.rating.average ? -1 : 1;
          case 'favorites':
            return itemA.favoriteCount < itemB.favoriteCount ? -1 : 1;
          case 'price':
            return itemA.portion.price < itemB.portion.price ? -1 : 1;
          case 'timestamp':
            return itemA.timestamp < itemB.timestamp ? -1 : 1;
          default:
            return itemA[this.sortBy] < itemB[this.sortBy] ? -1 : 1;
        }
      } else if (this.direction === 'desc') {
        switch (this.sortBy) {
          case 'rating':
            return itemA.rating.average < itemB.rating.average ? 1 : -1;
          case 'favorites':
            return itemA.favoriteCount < itemB.favoriteCount ? 1 : -1;
          case 'price':
            return itemA.portion.price < itemB.portion.price ? 1 : -1;
          case 'timestamp':
            return itemA.timestamp < itemB.timestamp ? 1 : -1;
          default:
            return itemA[this.sortBy] < itemB[this.sortBy] ? 1 : -1;
        }
      }
    });
  }
}
