import { RequestItem, RequestUser } from './request';

export interface Chat {
  createdAt: number;
  createdBy: RequestUser;
  item: RequestItem;
  messages: Message[];
  owner: string;
}

export interface Message {
  content: string;
  createdAt: number;
  user: RequestUser;
}
