import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Subject } from 'rxjs/internal/Subject';
import { switchMap, takeUntil } from 'rxjs/operators';

import { GeneralService } from '../../shared/services/general/general.service';
import { ProductService } from '../../shared/services/product/product.service';
import { UserService } from '../../shared/services/user/user.service';

import { Chat, Message } from './models/chat';
import { ChatService } from './services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnDestroy {

  public chat: Chat;
  public userId: string;
  public newMsg: string;
  public isLoading: boolean;

  private chatId: string;
  private unsubscribe$: Subject<void>;

  @ViewChild('chatDiv', { static: false }) private chatDiv: ElementRef;

  constructor(private chatService: ChatService,
              private generalService: GeneralService,
              private productService: ProductService,
              private userService: UserService,
              private navCtrl: NavController,
              private route: ActivatedRoute) {
    this.unsubscribe$ = new Subject<void>();
    this.isLoading = true;
    this.generalService.setScreenName('Chat');
  }

  public ngOnInit(): void {
    this.chatId = `${this.route.snapshot.paramMap.get('chatId')}`;

    this.userService.userId
      .pipe(
        switchMap((userId: string) => {
          this.userId = userId;

          return this.chatService.getChat(this.chatId).pipe(
            takeUntil(this.unsubscribe$),
          );
        }),
        takeUntil(this.unsubscribe$),
      )
      .subscribe((chat: Chat) => {
        this.chat = chat;
        this.isLoading = false;
      });
  }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  public async ionViewWillLeave(): Promise<void> {
    if (this.chat.messages.length === 0) {
      try {
        await this.chatService.deleteChat(this.chatId);
      } catch (e) {
        console.error('Error deleting msg:', e);
      }
    }
  }

  public back(): void {
    this.navCtrl.back();
  }

  public submit(): void {
    this.chatService.sendMessage(this.chatId, this.newMsg);
    this.newMsg = '';
  }

  public trackByCreated(i: number, msg: Message): number {
    return msg.createdAt;
  }
}
