import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IonRouterOutlet, ModalController, NavController } from '@ionic/angular';
import { Subject } from 'rxjs/internal/Subject';
import { take, takeUntil } from 'rxjs/operators';

import { FavoriteService } from '../../shared/services/favorite/favorite.service';
import { Product, ProductUnitEnum } from '../../shared/services/product/models/product';
import { ProductService } from '../../shared/services/product/product.service';
import { UserService } from '../../shared/services/user/user.service';
import { CreatePage } from '../create/create.page';

import { RequestFormComponent } from './partials/request-form/request-form.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit, OnDestroy {

  public item: Product;
  public slideOpts: unknown;
  public unitEnum: typeof ProductUnitEnum;
  public amount: number;
  public userId: string;

  private itemId: string;
  private onDestroy$: Subject<void>;

  constructor(private activatedRoute: ActivatedRoute,
              private navCtrl: NavController,
              private favoriteService: FavoriteService,
              private userService: UserService,
              private modalCtrl: ModalController,
              private routerOutlet: IonRouterOutlet,
              private router: Router,
              private productService: ProductService) {
    this.onDestroy$ = new Subject<void>();
    this.unitEnum = ProductUnitEnum;
    this.amount = 1;
    this.slideOpts = {
      speed: 400,
      slidesPerView: 'auto',
      spaceBetween: 10,
      scrollbar: {
        snapOnRelease: false,
      },
    };
  }

  public async ngOnInit(): Promise<void> {
    this.userService.userId
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((id: string) => {
        this.userId = id;
      });

    this.item = this.productService.selectedItem;

    if (!this.item) {
      const params: Params = await this.activatedRoute.params.pipe(take(1)).toPromise();
      this.itemId = params?.id;
      this.productService.getProductById(this.itemId)
        .pipe(takeUntil(this.onDestroy$))
        .subscribe((product: Product) => {
          console.debug('getProductById Sub', product);
          this.item = product;
        });
    }

  }

  public ngOnDestroy(): void {
    this.productService.selectedItem = undefined;
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  public async editItem(): Promise<void> {
    const modal: HTMLIonModalElement = await this.modalCtrl.create({
      component: CreatePage,
      componentProps: { item: this.item, editing: true },
      cssClass: 'edit-modal',
      swipeToClose: true,
      backdropDismiss: true,
      presentingElement: this.routerOutlet.nativeEl,
    });

    return await modal.present();
  }

  public async showRequestForm(): Promise<void> {
    // this.adMobService.hideBanner();
    const modal: HTMLIonModalElement = await this.modalCtrl.create({
      component: RequestFormComponent,
      componentProps: { item: this.item, portionAmount: this.amount },
      cssClass: 'edit-modal',
      swipeToClose: true,
      backdropDismiss: true,
      presentingElement: this.routerOutlet.nativeEl,
    });

    // modal.onWillDismiss().then( () => {
    //   this.adMobService.showInterstitial();
    // });
    //
    // modal.onDidDismiss().then( () => {
    //   this.adMobService.resumeBanner();
    // });

    return await modal.present();
  }

  public async favorite(): Promise<void> {
    await this.favoriteService.updateFavorite(this.item);
  }

  public showUserProfile(): void {
    this.router.navigateByUrl(`tabs/customer-profile/${this.item.user.id}`);
  }

  public back(): void {
    this.navCtrl.back();
  }

  public increase(): void {
    this.amount += 1;
  }

  public decrease(): void {
    if (this.amount <= 1) {
      this.amount = 1;
    } else {
      this.amount -= 1;
    }
  }

}
