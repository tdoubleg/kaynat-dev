export interface TabPage {
  title: string;
  url: string;
  icon: string;
  badgeCount?: number;
}
