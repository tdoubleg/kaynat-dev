import { Product } from '../../product/models/product';

export interface BaseUser {
  id: string;
  profileImageUrl: string;
  username: string;
}
export interface User extends BaseUser {
  firstName: string;
  lastName: string;
  email: string;
  verified: boolean;
  active: boolean;
  products: Product[];
}
