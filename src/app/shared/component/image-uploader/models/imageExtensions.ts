export const imageExtensions: string[] = [
  'jpeg',
  'jpg',
  'png',
  'heif',
  'heic',
];
