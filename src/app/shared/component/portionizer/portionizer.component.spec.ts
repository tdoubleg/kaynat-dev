import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PortionizerComponent } from './portionizer.component';

describe('PortionizerComponent', () => {
  let component: PortionizerComponent;
  let fixture: ComponentFixture<PortionizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortionizerComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PortionizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
