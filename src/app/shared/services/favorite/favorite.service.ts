import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentChangeAction,
  DocumentData,
} from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { share } from 'rxjs/operators';
import { GeneralService } from '../general/general.service';

import { Product } from '../product/models/product';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root',
})
export class FavoriteService {

  public favoriteItems: BehaviorSubject<Partial<Product>[]>;

  private favoritesRef: AngularFirestoreCollection;
  private isRegistered: boolean;

  constructor(private firestore: AngularFirestore,
              private generalService: GeneralService,
              private router: Router,
              private userService: UserService) {
    this.favoriteItems = new BehaviorSubject(null);

    this.userService.userId.subscribe((userId: string) => {
      if (userId) {
        this.isRegistered = true;
        this.favoritesRef = this.firestore.doc(`/user/${userId}`).collection('favorites');
        this.getFavorites();
      }
    });
  }

  public getFavorites(): void {
    this.favoritesRef.snapshotChanges()
      .pipe(share())
      .subscribe((favorites: DocumentChangeAction<DocumentData>[]) => {
        console.debug('getFavorites');
        if (!favorites) {
          this.favoriteItems.next([]);
        } else {
          const favoritesTemp: Partial<Product>[] = favorites.map((favorite: DocumentChangeAction<DocumentData>) =>
            ({ id: favorite.payload.doc.id, ...favorite.payload.doc.data() }));
          this.favoriteItems.next(favoritesTemp);
        }
      });
  }

  public async updateFavorite(item: Partial<Product>): Promise<void> {
    if (this.isRegistered) {
      if (this.isFavorised(item.id)) {
        await this.removeFavorite(item);
        // this.generalService.setLogEvent('on_favorite_remove', { placeId: place.id });
      } else {
        await this.setFavorite(item);
        // this.generalService.setLogEvent('on_favorite_add', { placeId: place.id });
      }
    } else {
      await this.generalService.presentConfirmAlertWithHandler(
        'general.login.title',
        'general.login.message',
        'general.login.button',
        async () => {
          await this.router.navigateByUrl('/login');
        },
      );
    }
  }

  public isFavorised(itemId: string): boolean {
    return this.favoriteItems.value?.find((favorite: Partial<Product>) => favorite.id === itemId) !== undefined;

  }

  public setFavorite(item: Partial<Product>): Promise<void> {
    const product: Partial<Product> = {
      id: item.id,
      name: item.name,
      images: item.images,
      user: item.user,
      rating: item.rating,
      favoriteCount: item.favoriteCount,
    };

    return this.favoritesRef.doc(product.id).set({ ...product });
  }

  private removeFavorite(item: Partial<Product>): Promise<void> {
    return this.favoritesRef.doc(item.id).delete();
  }
}
