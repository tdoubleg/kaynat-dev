import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireStorageReference } from '@angular/fire/storage';
import { Camera, CameraDirection, CameraPhoto, CameraResultType, CameraSource } from '@capacitor/core';
import { TranslateService } from '@ngx-translate/core';

import { Image } from '../models/image';
import { imageExtensions } from '../models/imageExtensions';

// tslint:disable:typedef
@Injectable({
  providedIn: 'root',
})
export class ImageService {

  public images: Image[];

  constructor(private firestore: AngularFirestore,
              private translateService: TranslateService,
              private storage: AngularFireStorage) {
    this.images = [];
  }

  /**
   * Calls the Capacitor Camera Plugin.<br>
   * Opens <i><b>File selection dialog</b></i> for Web and <i><b>Camera/Gallery</b></i> for iOS and Android<br>
   * <b>TODO: Open Camera for PWAs - missing PWA Element 'pwa-camera-modal'. See the docs:
   * https://capacitorjs.com/docs/pwa-elements</b>
   * @return Promise The result of the returned promise is an array of the type Image
   */
  public takePicture(): Promise<Image[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const capturedPhoto: CameraPhoto = await Camera.getPhoto({
          quality: 90,
          allowEditing: true,
          saveToGallery: true,
          source: CameraSource.Prompt,
          resultType: CameraResultType.Base64,
          correctOrientation: true,
          direction: CameraDirection.Rear,
          promptLabelHeader: this.translateService.instant('create.camera.promptLabelHeader'),
          promptLabelCancel: this.translateService.instant('create.camera.promptLabelHeader'),
          promptLabelPhoto: this.translateService.instant('create.camera.promptLabelHeader'),
          promptLabelPicture: this.translateService.instant('create.camera.promptLabelPicture'),
        });

        if (!imageExtensions.includes(capturedPhoto.format)) {
          reject('unsupported file type');
        } else {
          const base64Image: string = 'data:image/jpeg;base64,' + capturedPhoto.base64String;
          const blob: Blob = this.base64StringToBlob(base64Image);
          const file: File = new File([blob], Date.now().toString());
          this.images.unshift({ file, format: capturedPhoto.format, url: base64Image, loaded: false });
          resolve(this.images);
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  /**
   * Uploads images to Firebase Storage.<br>
   * You can set a filename on image selection <b>(Interface: Image, Property: fileName)</b><br>
   * If the image filename is missing, a random Id (Firestore.createId()) will be set.
   * @param path The path where to store the file, e.g. images/user/{userId}/{image.fileName}
   * @return Promise<string[]> URL Array of the uploaded files
   */
  public uploadImages(path: string): Promise<string[]> {
    const downloadUrls$: Promise<string>[] = [];

    this.images.forEach((image: Image) => {
      if (!image.fileName) {
        image.fileName = this.firestore.createId();
      }
      const filePath: string = `${path}/${image.fileName}.${image.format}`;
      const fileRef: AngularFireStorageReference = this.storage.ref(filePath);
      const task: Promise<string> = new Promise(async (resolve, reject) => {
        if (image.file) {
          try {
            await this.storage.upload(filePath, image.file);
            const downloadURL: string = await fileRef.getDownloadURL().toPromise();
            resolve(downloadURL);
          } catch (error) {
            console.error('Image Upload Error', [image, error]);
            reject(error);
          }
        } else {
          resolve(image.url);
        }
      });
      downloadUrls$.push(task);
    });

    return Promise.all(downloadUrls$);
  }

  /**
   * Convert a base64 string to a File-Type.<br>
   * Required to upload the image to firebase storage
   * @param dataURI The base64 string
   * @return Blob Returns the Blob of the file
   */
  public base64StringToBlob(dataURI: string): Blob {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    const byteString: string = atob(dataURI.split(',')[1]);

    // write the bytes of the string to an ArrayBuffer
    const ab: ArrayBuffer = new ArrayBuffer(byteString.length);
    const ia: Uint8Array = new Uint8Array(ab);
    for (let i: number = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    return new Blob([ab]);
  }
}
