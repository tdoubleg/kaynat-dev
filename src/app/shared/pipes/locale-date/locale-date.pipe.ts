import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'localeDate',
})
export class LocaleDatePipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  public transform(value: number, toDateTime: boolean): unknown {
    const _value: number = value * 1000;

    return toDateTime ? this.getFormattedDateTime(_value) : this.getFormattedDate(_value);
  }

  private getTimezone(): string {
    return moment.tz.guess();
  }

  private getFormattedDateTime(date: string | number): string {
    const dateTimeFormat: string = this.translateService.instant('general.dateTimeFormat');

    return this.toCorrectTimezone(date, dateTimeFormat);
  }

  private getFormattedDate(date: string | number): string {
    const dateFormat: string = this.translateService.instant('general.dateFormat');

    return this.toCorrectTimezone(date, dateFormat);
  }

  private toCorrectTimezone(date: string | number, dateFormat: string): string {
    return moment(date).tz(this.getTimezone()).format(dateFormat);
  }
}
