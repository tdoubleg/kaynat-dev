import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import firebase from 'firebase';
import { NgxFirebaseAuthService } from 'ngx-firebase-auth';
import { AuthContext } from 'ngx-firebase-auth/lib/models/authContext';

import { GeneralService } from '../../../../shared/services/general/general.service';
import { LoginService } from '../../services/login/login.service';
import FirebaseError = firebase.FirebaseError;

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {

  public form: FormGroup;
  public isLoading: boolean;

  constructor(private loginService: LoginService,
              private generalService: GeneralService,
              private authService: NgxFirebaseAuthService,
              private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  public ngOnInit(): void {
    this.initForm();
  }

  public initForm(): void {
    this.form = this.formBuilder.group({
      email: ['a@a.de', Validators.compose([Validators.email, Validators.required])],
      password: ['Test123!', Validators.required],
    });
  }

  public async loginWithEmail(isRegistering?: boolean): Promise<void> {
    this.isLoading = true;
    const authContext: AuthContext = {
      email: this.form.controls.email.value,
      password: this.form.controls.password.value,
    };

    try {
      if (isRegistering) {
        await this.authService.register(authContext);
      } else {
        await this.authService.login(authContext);
      }
      const returnUrl: string = this.activatedRoute.snapshot.queryParams.returnUrl || '/';
      await this.router.navigateByUrl(returnUrl, { replaceUrl: true });
    } catch (e) {
      const error: FirebaseError = e as FirebaseError;
      await this.generalService.presentFirebaseErrorAlert(error);
    } finally {
      this.isLoading = false;
    }
  }

  public async resetPassword(): Promise<void> {
    const emailForm: AbstractControl = this.form.controls.email;
    await this.loginService.getResetForm(emailForm);
  }

}
