export enum GeolocationErrorCodes {
  DENIED = 1,
  NO_RESPONSE = 2,
}
