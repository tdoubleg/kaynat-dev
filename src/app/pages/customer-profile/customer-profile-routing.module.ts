import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CustomerProfilePage } from './customer-profile.page';

const routes: Routes = [
  {
    path: ':userId',
    component: CustomerProfilePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerProfilePageRoutingModule {
}
