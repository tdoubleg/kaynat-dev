import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  public enableDarkMode: boolean;

  public ngOnInit(): void {
    this.enableDarkMode = JSON.parse(localStorage.getItem('darkMode'));
  }

  public setDarkMode(checked: boolean): void {
    this.enableDarkMode = checked;
    document.body.classList.toggle('dark', this.enableDarkMode);
    localStorage.setItem('darkMode', String(this.enableDarkMode));
    // this.generalService.setLogEvent('on_darkmode', { active: checked });
  }

  public openFacebook(): void {
    window.open('https://www.facebook.com/kenaraapp/', '_blank');
  }

}
