import { Injectable } from '@angular/core';
import { Action } from '@angular/fire/database';
import { AngularFirestore, AngularFirestoreDocument, DocumentSnapshot } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import * as moment from 'moment-timezone';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../../../shared/services/user/models/user';
import { UserService } from '../../../shared/services/user/user.service';
import { Chat, Message } from '../models/chat';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  constructor(private afs: AngularFirestore, private userService: UserService) {

  }

  // public getMessagesForUser(userId: string): Observable<any> {
  //   const own: Observable<DocumentChangeAction<Chat>[]> = this.afs.collection<Chat>('chats',
  //     (ref: CollectionReference<DocumentData>) => ref.where('owner', '==', userId)).snapshotChanges();
  //
  //   const participant: Observable<DocumentChangeAction<Chat>[]> = this.afs.collection<Chat>('chats',
  //     (ref: CollectionReference<DocumentData>) => ref.where('createdBy', '==', userId)).snapshotChanges();
  //
  //   return combineLatest([own, participant])
  //     .pipe(
  //       map((docs: any[]) => {
  //         // const newDocs = docs.flat();
  //         console.log(docs);
  //
  //         return docs.map((doc) => {
  //           const chat: Chat = {
  //             id: doc.payload.doc.id,
  //             ...doc.payload.doc.data(),
  //           };
  //
  //           return chat;
  //         }).filter((chat: Chat) => !!chat);
  //       }),
  //     );
  // }

  public getChat(chatId: string): Observable<Chat> {
    return this.afs.doc(`chats/${chatId}`).snapshotChanges().pipe(
      map((doc: Action<DocumentSnapshot<Chat>>) => {

        const chat: Chat = {
          requestId: chatId,
          ...doc.payload.data(),
        };

        return chat;
      }),
    );
  }

  public async deleteChat(chatId: string): Promise<void> {
    return await this.afs.collection('chats').doc(chatId).delete();
  }

  public async sendMessage(chatId: string, content: string): Promise<void> {
    const user: User = await this.userService.user$.value;

    const data: Message = {
      content,
      createdAt: moment().unix(),
      user: {
        id: user.id,
        profileImageUrl: user.profileImageUrl,
        username: user.username,
      },
    };

    if (user) {
      const ref: AngularFirestoreDocument<unknown> = this.afs.collection('chats').doc(chatId);

      return ref.update({
        messages: firebase.default.firestore.FieldValue.arrayUnion(data),
      });
    }
  }
}
