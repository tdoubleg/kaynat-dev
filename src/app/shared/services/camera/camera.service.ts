import { Injectable } from '@angular/core';
import { CameraDirection, CameraPhoto, CameraResultType, FilesystemDirectory, Plugins } from '@capacitor/core';
import { TranslateService } from '@ngx-translate/core';

const { Camera, Filesystem, Storage } = Plugins;

@Injectable({
  providedIn: 'root',
})
export class CameraService {

  constructor(private translateService: TranslateService) {
  }

  // public takePicture(): Promise<string> {
  //   return new Promise(async (resolve, reject) => {
  //     try {
  //       const image: CameraPhoto = await Camera.getPhoto({
  //         quality: 90,
  //         allowEditing: true,
  //         saveToGallery: true,
  //         resultType: CameraResultType.Base64,
  //         correctOrientation: true,
  //         direction: CameraDirection.Rear,
  //         promptLabelHeader: this.translateService.instant('create.camera.promptLabelHeader'),
  //         promptLabelCancel: this.translateService.instant('create.camera.promptLabelHeader'),
  //         promptLabelPhoto: this.translateService.instant('create.camera.promptLabelHeader'),
  //         promptLabelPicture: this.translateService.instant('create.camera.promptLabelPicture'),
  //       });
  //       resolve(image.base64String);
  //     } catch (error) {
  //       reject(error);
  //     }
  //   });
  // }
}
