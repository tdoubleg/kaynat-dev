import { NgModule } from '@angular/core';

import { UserProfileModule } from '../../shared/component/user-profile/user-profile.module';
import { SharedModule } from '../../shared/shared-module/shared.module';

import { ChatRouting } from './chat-routing.module';
import { ChatComponent } from './chat.component';

@NgModule({
  declarations: [
    ChatComponent,
  ],
  imports: [
    SharedModule,
    ChatRouting,
    UserProfileModule,
  ],
})
export class ChatModule {
}
