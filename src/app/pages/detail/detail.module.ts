import { NgModule } from '@angular/core';
import { RatingModule } from 'ng-starrating';
import { PortionizerModule } from '../../shared/component/portionizer/portionizer.module';

import { SharedModule } from '../../shared/shared-module/shared.module';

import { DetailPageRoutingModule } from './detail-routing.module';
import { DetailPage } from './detail.page';
import { RequestFormComponent } from './partials/request-form/request-form.component';
import { IsFavoritePipe } from './pipes/favorite/is-favorite.pipe';

@NgModule({
  imports: [
    SharedModule,
    RatingModule,
    DetailPageRoutingModule,
    PortionizerModule,
  ],
  declarations: [
    DetailPage,
    RequestFormComponent,
    IsFavoritePipe,
  ],
})
export class DetailPageModule {
}
