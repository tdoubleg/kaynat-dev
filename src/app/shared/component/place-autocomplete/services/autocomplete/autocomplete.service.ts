import { Injectable } from '@angular/core';

import { LocaleService } from '../../../../services/locale/locale.service';
import Geocoder = google.maps.Geocoder;
import GeocoderRequest = google.maps.GeocoderRequest;
import GeocoderResult = google.maps.GeocoderResult;
import GeocoderStatus = google.maps.GeocoderStatus;
import LatLng = google.maps.LatLng;

@Injectable({
  providedIn: 'root',
})
export class AutocompleteService {
  private geocoder: Geocoder;

  constructor(private localeService: LocaleService) {
    this.geocoder = new Geocoder();
  }

  public geocode(search: string | LatLng): Promise<GeocoderResult[]> {
    let request: GeocoderRequest;
    if (typeof search === 'string') {
      request = {
        address: search,
        componentRestrictions: {
          country: this.localeService.locale,
        },
      };
    } else {
      request = {
        location: search,
      };
    }

    // tslint:disable-next-line:typedef
    return new Promise((resolve, reject) => {
      this.geocoder.geocode(request, (results: GeocoderResult[], status: GeocoderStatus) => {
        if (status === 'OK') {
          const filteredResults: GeocoderResult[] = results.filter((result: GeocoderResult) => result.types.includes('postal_code'));
          resolve(filteredResults);
        } else {
          reject('geocodeByTerm() was not successful for the following reason: ' + status);
        }
      });
    });
  }
}
