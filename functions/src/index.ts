import * as admin from 'firebase-admin';
import { firestore } from 'firebase-admin';
import * as functions from 'firebase-functions';

import { newMessage } from './chat';
import { setStatusOnCreate, setStatusOnUpdate } from './requestStatus';
import { updateItemOnDeleteFavorise, updateItemOnFavorise } from './updateItem';
import QueryDocumentSnapshot = admin.firestore.QueryDocumentSnapshot;
import Firestore = firestore.Firestore;

admin.initializeApp();
const db: Firestore = admin.firestore();

// tslint:disable-next-line:no-any
const cloudFunction: any = functions.region('europe-west3').firestore;

// exports.updateUserInfo =
//   cloudFunction
//     .document('user/{userId}')
//     .onUpdate((change: Change<QueryDocumentSnapshot>, context: EventContext) => {
//       // TODO: IF PROFILE IMAGE CHANGED, CHANGE EVERY DATA WHERE PROFILE IMG IS USED
//       // in collections: deleted_products.user.profileImageUrl, products.user.profileImageUrl, requests.fromUser
//     });

exports.updateItemOnFavorise =
  cloudFunction
    .document('user/{userId}/favorites/{docId}')
    .onCreate((change: QueryDocumentSnapshot) => updateItemOnFavorise(db, change));

exports.updateItemOnDeleteFavorise =
  cloudFunction
    .document('user/{userId}/favorites/{docId}')
    .onDelete((change: QueryDocumentSnapshot) => updateItemOnDeleteFavorise(db, change));

exports.setRequestStatusOnCreate =
  cloudFunction
    .document('requests/{requestId}')
    .onCreate((snapshot: QueryDocumentSnapshot) => setStatusOnCreate(db, snapshot));

exports.changeRequestStatus =
  cloudFunction
    .document('requests/{requestId}')
    .onUpdate(setStatusOnUpdate);

exports.newMessage =
  cloudFunction
    .document('chats/{chatId}')
    .onUpdate(newMessage);
