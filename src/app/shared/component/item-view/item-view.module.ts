import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared-module/shared.module';
import { UserProfileModule } from '../user-profile/user-profile.module';

import { ItemViewComponent } from './item-view.component';

@NgModule({
  declarations: [
    ItemViewComponent,
  ],
  exports: [
    ItemViewComponent,
  ],
  imports: [
    SharedModule,
    UserProfileModule,
  ],
})
export class ItemViewModule {
}
