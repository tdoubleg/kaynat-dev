import { Component } from '@angular/core';
import { AnimationController } from '@ionic/angular';

import { Animations } from '../../shared/animations/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  public shouldShowForm: boolean;
  public shouldShowLoginForm: boolean;
  public shouldShowRegisterForm: boolean;

  constructor(private animationCtrl: AnimationController) {
  }

  public showForm(form: 'login' | 'register'): void {
    this.shouldShowForm = true;
    this.shouldShowLoginForm = form === 'login';
    this.shouldShowRegisterForm = form === 'register';

    setTimeout(async () => {
      await new Animations(this.animationCtrl).slideInOut().play();
    }, 1);

  }

  public async closeForm(): Promise<void> {
    await new Animations(this.animationCtrl).slideInOut(true).play();
    this.shouldShowForm = false;
  }

  public googleLogin(): void {
    console.debug('googleLogin');
  }

  public facebookLogin(): void {
    console.debug('facebookLogin');
  }

  public appleLogin(): void {
    console.debug('appleLogin');
  }
}
