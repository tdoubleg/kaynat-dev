import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { TranslateService } from '@ngx-translate/core';
import firebase from 'firebase';
import { NgxFirebaseAuthService } from 'ngx-firebase-auth';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs/internal/Subject';
import { map, share, shareReplay, takeUntil } from 'rxjs/operators';

import { ImageService } from '../../component/image-uploader/services/image.service';
import { GeneralService } from '../general/general.service';

import { User } from './models/user';
import UserCredential = firebase.auth.UserCredential;

@Injectable({
  providedIn: 'root',
})
export class UserService {

  public user$: BehaviorSubject<User>;

  public destroy$: Subject<void>;

  constructor(private firestore: AngularFirestore,
              private translateService: TranslateService,
              private generalService: GeneralService,
              private imageService: ImageService,
              private authService: NgxFirebaseAuthService) {
    this.user$ = new BehaviorSubject<User>(null);
    this.destroy$ = new Subject<void>();
  }

  public get userId(): Observable<string> {
    return this.user$.pipe(
      share(),
      takeUntil(this.destroy$),
      map((user: User) => user ? user.id : null),
    );
  }

  public setUserInformation(user: User): Promise<void> {
    const { id, ...userPartial } = user;

    return this.firestore.doc(`user/${id}`).set(userPartial, { merge: true });
  }

  public async setProfileImage(userId: string): Promise<string> {
    try {
      const images: string[] = await this.imageService.uploadImages(`user/${userId}`);

      return images[0];
    } catch (e) {
      console.error('Error uploading profile image', e);
    }

  }

  public getUserInformation(userId: string): void {
    this.firestore.doc(`user/${userId}`).valueChanges()
      .pipe(
        shareReplay(),
        takeUntil(this.destroy$),
      ).subscribe(async (user: User) => {

      const dbUser: User = {
        ...user,
        id: userId,
        email: this.authService.currentUser?.email,
      };

      if (user) {
        await this.updateVerifiedStatus(dbUser);
      }
      console.debug('getUserInformation$', dbUser);
      this.user$.next(dbUser);
    });
  }

  public async updateEMail(password: string, email: string): Promise<void> {
    try {
      const firebaseUser: firebase.User = await this.authService.reauthenticateUser(password);
      await firebaseUser.updateEmail(email);
    } catch (e) {
      console.error(e);
      await this.generalService.presentInfoAlert('user.email.error.header', 'user.email.error.message', e);
    }
  }

  public updateProfile(userCredential: UserCredential, displayName: string, photoURL: string): Promise<void> {
    return userCredential.user.updateProfile({ displayName, photoURL });
  }

  public async updateVerifiedStatus(user: User): Promise<void> {
    if (user.verified !== this.authService.currentUser.emailVerified) {
      try {
        await this.firestore.doc(`user/${user.id}`)
          .update({ verified: this.authService.currentUser.emailVerified });
      } catch (e) {
        console.error('Update verified Status', e);
      }
    }
  }

}
