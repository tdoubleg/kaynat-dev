import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

import { FavoriteService } from '../../../../shared/services/favorite/favorite.service';
import { Product } from '../../../../shared/services/product/models/product';

@Pipe({
  name: 'isFavorite',
})
export class IsFavoritePipe implements PipeTransform {

  constructor(private favoriteService: FavoriteService) {
  }

  public transform(item: Product | Partial<Product>): Observable<string> {
    return this.favoriteService.favoriteItems.pipe(
      map((products: Partial<Product>[]) => {
        if (!products || products.length === 0) {
          return 'heart-outline';
        }

        return this.favoriteService.isFavorised(item.id) ? 'heart-sharp' : 'heart-outline';
      }),
    );
  }

}
