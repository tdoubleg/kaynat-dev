import { Component, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

import { Product } from '../../../../shared/services/product/models/product';
import { ProductService } from '../../../../shared/services/product/product.service';

const MARGIN_RIGHT: number = 30;

@Component({
  selector: 'app-recommended-slider',
  templateUrl: './recommended-slider.component.html',
  styleUrls: ['./recommended-slider.component.scss'],
})
export class RecommendedSliderComponent {
  @Input() public title: string;
  @Input() public items: Product[];
  @Input() public type: string;

  public slideOpts: unknown;

  constructor(private productService: ProductService, private navCtrl: NavController) {
    this.slideOpts = {
      speed: 400,
      spaceBetween: 5,
      width: window.innerWidth - MARGIN_RIGHT,
      scrollbar: {
        snapOnRelease: false,
      },
    };
  }

  public showProductDetail(item: Product): void {
    this.productService.selectedItem = item;
    this.navCtrl.navigateForward(`/tabs/detail/${item.id}`).then();
  }
}
