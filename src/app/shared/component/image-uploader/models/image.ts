export interface Image {
  url: string;
  format?: string;
  file?: File;
  loaded?: boolean;
  fileName?: string;
}
