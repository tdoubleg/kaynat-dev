import { Input, OnChanges, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { IonItemSliding, NavController } from '@ionic/angular';
import { Subject } from 'rxjs/internal/Subject';

import { GeneralService } from '../../../shared/services/general/general.service';
import { User } from '../../../shared/services/user/models/user';
import { ChatService } from '../../chat/services/chat.service';
import { Request, RequestStatusEnum } from '../../detail/service/request/models/request';
import { RequestService } from '../../detail/service/request/request.service';

export abstract class BaseRequestComponent implements OnChanges {

  @Input() public user: User;
  @ViewChildren(IonItemSliding) public slider: QueryList<IonItemSliding>;

  public requests: Request[];
  public filteredRequests: Request[];
  public requestStatusEnum: typeof RequestStatusEnum;
  public currentState: RequestStatusEnum;

  protected destroy$: Subject<void>;

  constructor(protected requestService: RequestService,
              protected chatService: ChatService,
              protected navCtrl: NavController,
              protected generalService: GeneralService) {
    this.destroy$ = new Subject<void>();
    this.requestStatusEnum = RequestStatusEnum;
    this.currentState = RequestStatusEnum.PENDING;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (this.user) {
      this.getItems();
    }
  }

  public ionViewWillLeave(): void {
    this.destroy$.next();
  }

  public getItems(): void {
    return;
  }

  public async openChat(request: Request): Promise<void> {
    try {
      await this.requestService.updateRequestReadStatus(request.id);
      await this.navCtrl.navigateForward(['tabs/chat', request.id]);
    } catch (e) {
      await this.generalService.presentLoginAlert(async () => {
          await this.navCtrl.navigateForward('/login');
        },
      );
    }
  }

  public segmentChanged(segment: CustomEvent): void {
    this.currentState = Number(segment.detail.value);
    this.filterRequests();
  }

  protected filterRequests(): void {
    if (this.requests) {
      this.filteredRequests = this.requests.filter((request: Request) => this.currentState === request.status);
    }
  }
}
