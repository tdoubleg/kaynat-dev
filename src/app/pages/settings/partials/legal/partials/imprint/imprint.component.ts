import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-imprint',
  templateUrl: './imprint.component.html',
  styleUrls: ['./imprint.component.scss'],
})
export class ImprintComponent {

  constructor(private modalCtrl: ModalController) { }

  public close(): void {
    this.modalCtrl.dismiss();
  }

}
